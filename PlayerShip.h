//Arne Sercu
//1DAE17

#pragma once
#include "Ship.h"

class PlayerShip : public Ship
{
public:
	~PlayerShip();
	explicit PlayerShip(int damageState, Color c);
	virtual void Update(Vector2f wind, float elapsedSec, const Level& level) override;
private:
	bool m_PressedLeft;
	bool m_PressedRight;
};

