//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "Island.h"
#include "SVGParser.h"
#include "utils.h"

Island::Island(float x, float y, IslandType type)
{
	m_Pos = Point2f(x, y);
	m_Type = type;
}

Island::Island(const Island& i)
{
	m_Pos = i.m_Pos;
	m_Type = i.m_Type;

	switch (m_Type)
	{
	case type1:
		m_upTex = std::make_unique<Texture>("resources/images/island1.png");
		m_upFortTex = std::make_unique<Texture>("resources/images/fort1.png");
		SVGParser::GetVerticesFromSvgFile("resources/SVG/island1.svg", m_Vertices);
		SVGParser::GetVerticesFromSvgFile("resources/SVG/fort1.svg", m_FortVertices);
		m_FortCannons.push_back(FortCannon(Point2f(160.0f, 160.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(160.0f, 288.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(160.0f, 416.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(350.0f, 288.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(350.0f, 416.0f), 500.0f));
		break;
	case type2:
		m_upTex = std::make_unique<Texture>("resources/images/island2.png");
		m_upFortTex = std::make_unique<Texture>("resources/images/fort2.png");
		SVGParser::GetVerticesFromSvgFile("resources/SVG/island2.svg", m_Vertices);
		SVGParser::GetVerticesFromSvgFile("resources/SVG/fort2.svg", m_FortVertices);
		m_FortCannons.push_back(FortCannon(Point2f(96.0f, 288.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(96.0f, 416.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(224.0f, 416.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(288.0f, 416.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(416.0f, 416.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(416.0f, 224.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(416.0f, 96.0f), 500.0f));
		break;
	case type3:
		m_upTex = std::make_unique<Texture>("resources/images/island3.png");
		m_upFortTex = std::make_unique<Texture>("resources/images/fort3.png");
		SVGParser::GetVerticesFromSvgFile("resources/SVG/island3.svg", m_Vertices);
		SVGParser::GetVerticesFromSvgFile("resources/SVG/fort3.svg", m_FortVertices);
		m_FortCannons.push_back(FortCannon(Point2f(96.0f, 288.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(224.0f, 416.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(224.0f, 160.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(350.0f, 288.0f), 500.0f));
		break;
	case type4:
		m_upTex = std::make_unique<Texture>("resources/images/island4.png");
		m_upFortTex = std::make_unique<Texture>("resources/images/fort4.png");
		SVGParser::GetVerticesFromSvgFile("resources/SVG/island4.svg", m_Vertices);
		SVGParser::GetVerticesFromSvgFile("resources/SVG/fort4.svg", m_FortVertices);
		m_FortCannons.push_back(FortCannon(Point2f(96.0f, 160.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(96.0f, 288.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(96.0f, 416.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(224.0f, 160.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(224.0f, 416.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(352.0f, 160.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(352.0f, 288.0f), 500.0f));
		m_FortCannons.push_back(FortCannon(Point2f(352.0f, 416.0f), 500.0f));
	default:
		break;
	}

	for (size_t i = 0; i < m_Vertices.size(); ++i)
	{
		m_Vertices[i] = Point2f(m_Vertices[i].x + m_Pos.x, m_Vertices[i].y + m_Pos.y);
	}
	for (size_t i = 0; i < m_FortVertices.size(); ++i)
	{
		m_FortVertices[i] = Point2f(m_FortVertices[i].x + m_Pos.x, m_FortVertices[i].y + m_Pos.y);
	}
	for (size_t i = 0; i < m_FortCannons.size(); ++i)
	{
		m_FortCannons[i].SetPosition(Point2f(m_FortCannons[i].GetPosition().x + m_Pos.x, m_FortCannons[i].GetPosition().y + m_Pos.y));
	}
}

void Island::Draw() const
{
	m_upTex->Draw(Rectf(m_Pos.x, m_Pos.y, m_upTex->GetWidth(), m_upTex->GetHeight()));
	m_upFortTex->Draw(Rectf(m_Pos.x, m_Pos.y, m_upTex->GetWidth(), m_upTex->GetHeight()));
	for (const FortCannon &fc : m_FortCannons)
	{
		fc.Draw();
	}
}

void Island::Update(float elapsedsec, Point2f shipPos)
{
	for (FortCannon &fc : m_FortCannons)
	{
		fc.Update(elapsedsec, shipPos);
	}
}

std::vector<Point2f> Island::GetVertices() const
{
	return m_Vertices;
}

Point2f Island::GetPos() const
{
	return m_Pos;
}

std::vector<Point2f> Island::GetFortVertices() const
{
	return m_FortVertices;
}

std::vector<FortCannon>& Island::GetCannons()
{
	return m_FortCannons;
}

void Island::EraseCannon(int index)
{
	m_FortCannons.erase(m_FortCannons.begin() + index);
}

