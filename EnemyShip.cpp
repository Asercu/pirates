//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "EnemyShip.h"
#include "Matrix2x3.h"

EnemyShip::~EnemyShip()
{
}

EnemyShip::EnemyShip(int damageState)
	: Ship(damageState, Color::gray)
	, m_LeftAnticrashOriginal{60.0f, -20.0f}
	, m_RightAnticrashOriginal{-60.0f, -20.0f}
{
}

EnemyShip::EnemyShip(const EnemyShip & e)
	: Ship(e.m_DamageState, Color::gray)
	, m_LeftAnticrashOriginal{ 60.0f, -20.0f }
	, m_RightAnticrashOriginal{ -60.0f, -20.0f }
{
	m_Rotation = 0.0f;
	m_CooldownLeft = 10.0f;
	m_CooldownRight = 10.0f;
	m_RightCannonSoundTimer = 10.0f;
	m_LeftCannonSoundTimer = 10.0f;
	m_RightCannonShotsLeft = 0;
	m_LeftCannonShotsLeft = 0;
}

void EnemyShip::Update(Vector2f wind, float elapsedSec, Level & level, Point2f playerPos, std::vector<std::shared_ptr<EnemyShip>> enemyShips)
{
	if (Vector2f(GetShipCenter(), playerPos).Length() > 1500.0f)
	{
		m_Speed = 0.0f;
		return;
	}
	
	//updates anticrash positions
	Matrix2x3 mt = Matrix2x3::CreateTranslationMatrix(GetShipCenter().x, GetShipCenter().y);
	Matrix2x3 mr = Matrix2x3::CreateRotationMatrix(m_Rotation);
	Matrix2x3 mw = mt * mr;
	m_LeftAnticrash = mw.Transform(m_LeftAnticrashOriginal);
	m_RightAnticrash = mw.Transform(m_RightAnticrashOriginal);

	//movement
	{
		// check if will crash
		bool avoidCrash = false;
		float Rotation = (m_Rotation + 90.0f) * 3.14159565f / 180.0f;

		Point2f straightAheadLeft = Point2f(m_LeftAnticrash.x - 600.0f * std::cos(Rotation), m_LeftAnticrash.y - 600.0f * std::sin(Rotation));
		Point2f straightAheadRight = Point2f(m_RightAnticrash.x - 600.0f * std::cos(Rotation), m_RightAnticrash.y - 600.0f * std::sin(Rotation));
		Point2f left = Point2f(GetShipCenter().x - 1500.0f * std::cos(Rotation + 3.14159565f / 16.0f), GetShipCenter().y - 1500.0f * std::sin(Rotation + 3.14159565f / 16.0f));
		Point2f right = Point2f(GetShipCenter().x - 1500.0f * std::cos(Rotation - 3.14159565f / 16.0f), GetShipCenter().y - 1500.0f * std::sin(Rotation - 3.14159565f / 16.0f));
		utils::HitInfo crash;
		for (std::shared_ptr<EnemyShip>& enemy : enemyShips)
		{
			if (utils::Raycast(enemy->GetCollision(), m_LeftAnticrash, straightAheadLeft, crash) || utils::Raycast(enemy->GetCollision(), m_RightAnticrash, straightAheadLeft, crash))
			{
				avoidCrash = true;
				m_Speed = 2;
				utils::HitInfo leftInfo;
				utils::HitInfo rightInfo;
				utils::Raycast(enemy->GetCollision(), m_LeftAnticrash, left, leftInfo);
				utils::Raycast(enemy->GetCollision(), m_RightAnticrash, right, rightInfo);
				if (leftInfo.lambda < rightInfo.lambda)
				{
					m_Rotation += m_ShipRotationSpeed * elapsedSec;
				}
				else
				{
					m_Rotation -= m_ShipRotationSpeed * elapsedSec;
				}
			}
		}
		for (Island& island : level.GetIslands())
		{
			if (utils::Raycast(island.GetVertices(), m_LeftAnticrash, straightAheadLeft, crash) || utils::Raycast(island.GetVertices(), m_RightAnticrash, straightAheadLeft, crash))
			{
				avoidCrash = true;
				m_Speed = 2;
				utils::HitInfo leftInfo;
				utils::HitInfo rightInfo;
				utils::Raycast(island.GetVertices(), m_LeftAnticrash, left, leftInfo);
				utils::Raycast(island.GetVertices(), m_RightAnticrash, right, rightInfo);
				if (leftInfo.lambda < rightInfo.lambda)
				{
					m_Rotation += m_ShipRotationSpeed * elapsedSec;
				}
				else
				{
					m_Rotation -= m_ShipRotationSpeed * elapsedSec;
				}
			}
		}
		if (!avoidCrash)
		{
			//rotate
			Vector2f toship = Vector2f(GetShipCenter(), playerPos);
			float angle2 = Vector2f(1.0f, 0.0f).AngleWith(toship) * 180.0f / 3.14159565f + 90.0f + (toship.Length() < 400.0f ? (rand() / 2 == 0 ? 90.0f : (-90.0f)) : 0);
			if (angle2 > 360.0f) angle2 -= 360.0f;
			if (angle2 < 0.0f) angle2 += 360.0f;
			if (m_Rotation > 360.0f)
				m_Rotation -= 360.0f;
			if (m_Rotation < 0.0f)
				m_Rotation += 360.0f;
			//current aimkwadrant, current shipkwadrant
			std::pair<int, int> kwadrants;
			if (m_Rotation < 90.0f) kwadrants.first = 1;
			else if (m_Rotation < 180.0f) kwadrants.first = 2;
			else if (m_Rotation < 270.0f) kwadrants.first = 3;
			else kwadrants.first = 4;
			if (angle2 < 90.0f) kwadrants.second = 1;
			else if (angle2 < 180.0f) kwadrants.second = 2;
			else if (angle2 < 270.0f) kwadrants.second = 3;
			else kwadrants.second = 4;

			if (kwadrants.first == kwadrants.second)
			{
				if (m_Rotation > angle2)
				{
					m_Rotation -= m_ShipRotationSpeed * elapsedSec;
				}
				else
				{
					m_Rotation += m_ShipRotationSpeed * elapsedSec;
				}
			}
			else if (kwadrants.second - kwadrants.first == 1 || (kwadrants.first == 4 && kwadrants.second == 1))
			{
				m_Rotation += m_ShipRotationSpeed * elapsedSec;
			}
			else if (kwadrants.first - kwadrants.second == 1 || (kwadrants.second == 4 && kwadrants.first == 1))
			{
				m_Rotation -= m_ShipRotationSpeed * elapsedSec;
			}
			else
			{
				if (abs((m_Rotation + 360.0f) - angle2) > abs((angle2 - m_Rotation)))
				{
					m_Rotation += m_ShipRotationSpeed * elapsedSec;
				}
				else
				{
					m_Rotation -= m_ShipRotationSpeed * elapsedSec;
				}
			}


			//set speed
			if (std::abs(m_Rotation - angle2) < 30.0f)
			{
				if (toship.Length() > 600.0f)
				{
					m_Speed = 3.0f;
				}

				else if (toship.Length() > 500.0f)
				{
					m_Speed = 2.0f;
				}
				else if (toship.Length() > 400.0f)
				{
					m_Speed = 1.0f;
				}
				else
				{
					m_Speed = 0.0f;
				}
			}
			else if (std::abs(m_Rotation - angle2) < 60.0f)
			{
				if (toship.Length() > 500.0f)
				{
					m_Speed = 2.0f;
				}
				else if (toship.Length() > 400.0f)
				{
					m_Speed = 1.0f;
				}
				else
				{
					m_Speed = 0.0f;
				}
			}
			else if (std::abs(m_Rotation - angle2) < 90.0f)
			{
				if (toship.Length() > 400.0f)
				{
					m_Speed = 1.0f;
				}
				else
				{
					m_Speed = 0.0f;
				}
			}
			else
			{
				m_Speed = 0.0f;
			}
		}
	}
		
	Ship::Update(wind, elapsedSec, level);
	if (utils::IsOverlapping(m_RangeLeft, Circlef(playerPos, 75.0f))) FireLeft();
	if (utils::IsOverlapping(m_RangeRight, Circlef(playerPos, 75.0f))) FireRight();
}

void EnemyShip::Draw() const
{
	Ship::Draw();
}

void EnemyShip::HandleFiring(float elapsedSec)
{
	m_RightCannonSoundTimer += elapsedSec;
	m_LeftCannonSoundTimer += elapsedSec;
	if (m_RightCannonShotsLeft > 0 && m_RightCannonSoundTimer > m_CannonSoundDelay)
	{
		--m_RightCannonShotsLeft;
		CannonBallManager::GetInstance().Fire(m_BallSpawnPointsRight[m_RightCannonShotsLeft], m_BulletVelocityRight, Source::EnemyShips);
		m_RightCannonSoundTimer = 0.0f;
	}
	if (m_LeftCannonShotsLeft > 0 && m_LeftCannonSoundTimer > m_CannonSoundDelay)
	{
		--m_LeftCannonShotsLeft;
		CannonBallManager::GetInstance().Fire(m_BallSpawnPointsLeft[m_LeftCannonShotsLeft], m_BulletVelocityLeft, Source::EnemyShips);
		m_LeftCannonSoundTimer = 0.0f;
	}
}


