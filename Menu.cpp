//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "Menu.h"
#include <sstream>
#include <iomanip>
#include "ScoreManager.h"
#include <map>


Menu::Menu(float windowWidth, float windowHeight)
	: m_FontString{ "Resources/Font/BlackSamsGold.ttf" }
	, m_WindowHeight{ windowHeight }
	, m_WindowWidth{ windowWidth }
	, m_ActiveColor{ Color::green }
	, m_ClickSound{ "resources/Sounds/Click.ogg" }
{	
	m_spShipsheet = std::make_shared<Texture>("Resources/images/shipsMiscellaneous_sheet.png");
	m_upTitle = std::make_unique<Texture>("Pirates", m_FontString, (int)(m_WindowHeight * 0.233f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upArrowRight = std::make_unique<Texture>(">", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upArrowLeft = std::make_unique<Texture>("<", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upPlay = std::make_unique<Texture>("Play", m_FontString, (int)(m_WindowHeight * 0.12f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upArrowRightHover = std::make_unique<Texture>(">", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.75, 0.0f, 0.0f, 1.0f));
	m_upArrowLeftHover = std::make_unique<Texture>("<", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.75f, 0.0f, 0.0f, 1.0f));
	m_upPlayHover = std::make_unique<Texture>("Play", m_FontString, (int)(m_WindowHeight * 0.12f), Color4f(0.75f, 0.0f, 0.0f, 1.0f));
	m_ShipInfo = SpriteReader::GetInstance().GetFullShip(m_ActiveColor);
	m_ArrowLeftDest = Rectf((m_WindowWidth / 2.0f - 1.5f * m_ShipInfo.width) / 2.0f - m_upArrowLeft->GetWidth() - 0.03f * m_WindowWidth, 0.6f * m_WindowHeight - 1.5f * m_ShipInfo.height / 2.0f - m_upArrowLeft->GetHeight() / 2.0f, m_upArrowLeft->GetWidth(), m_upArrowRight->GetHeight());
	m_ArrowRightDest = Rectf((m_WindowWidth / 2.0f - 1.5f * m_ShipInfo.width) / 2.0f + m_ShipInfo.width * 1.5f + 0.03f * m_WindowWidth, 0.6f * m_WindowHeight - 1.5f * m_ShipInfo.height / 2.0f - m_upArrowLeft->GetHeight() / 2.0f, m_upArrowLeft->GetWidth(), m_upArrowRight->GetHeight());
	m_PlayDest = Rectf((m_WindowWidth / 2.0f - m_upPlay->GetWidth()) / 2, 0.0833f * m_WindowWidth, m_upPlay->GetWidth(), m_upPlay->GetHeight());
	m_upControlsTitle = std::make_unique<Texture>("Learn to play", m_FontString, (int)(m_WindowHeight * 0.14f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upControlsMovement = std::make_unique<Texture>("resources/images/ControlsMovement.png");
	m_upControlsFire = std::make_unique<Texture>("resources/images/ControlsFire.png");
	m_upLearnToPlay = std::make_unique<Texture>("Learn to play", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upLearnToPlayHover = std::make_unique<Texture>("Learn to play", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.75f, 0.0f, 0.0f, 1.0f));
	m_LearnToPlayDest = Rectf(m_WindowWidth * 0.7f - m_upLearnToPlay->GetWidth() / 2.0f, m_WindowHeight * 0.44f, m_upLearnToPlay->GetWidth(), m_upLearnToPlay->GetHeight());
	m_LearnArrowRightDest = Rectf(m_WindowWidth - 30.0f - m_upArrowRight->GetWidth(), 0.0f, m_upArrowRight->GetWidth(), m_upArrowRight->GetHeight());
	m_upMainMenu = std::make_unique<Texture>("Main Menu", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upMainMenuHover = std::make_unique<Texture>("Main Menu", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.75f, 0.0f, 0.0f, 1.0f));
	m_MainMenuDest = Rectf((m_WindowWidth - m_upMainMenu->GetWidth()) / 2.0f, 0.0f, m_upMainMenu->GetWidth(), m_upMainMenu->GetHeight());
	m_LearnArrowLeftDest = Rectf(30.0f, 0.0f, m_upArrowRight->GetWidth(), m_upArrowRight->GetHeight());
	m_upControlsPickups = std::make_unique<Texture>("resources/images/ControlsPickups.png");
	m_upQuit = std::make_unique<Texture>("Quit", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upQuitHover = std::make_unique<Texture>("Quit", m_FontString, (int)(m_WindowHeight * 0.14f), Color4f(0.75f, 0.0f, 0.0f, 1.0f));
	m_upHighscores = std::make_unique<Texture>("Highscores", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upHighscoresHover = std::make_unique<Texture>("Highscores", m_FontString, (int)(m_WindowHeight * 0.14f), Color4f(0.75f, 0.0f, 0.0f, 1.0f));
	m_QuitDest = Rectf(m_WindowWidth * 0.7f - m_upQuit->GetWidth() / 2.0f, m_WindowHeight * 0.18f, m_upQuit->GetWidth(), m_upQuit->GetHeight());
	m_HighScoresDest = Rectf(m_WindowWidth * 0.7f - m_upHighscores->GetWidth() / 2.0f, m_WindowHeight * 0.31f, m_upHighscores->GetWidth(), m_upHighscores->GetHeight());
	m_upHighscoresTitle = std::make_unique<Texture>("Highscores", m_FontString, (int)(m_WindowHeight * 0.14f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upGameOverTitle = std::make_unique<Texture>("Game Over", m_FontString, (int)(m_WindowHeight * 0.14f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upRetryHover = std::make_unique<Texture>("Retry", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.75f, 0.0f, 0.0f, 1.0f));
	m_upRetry = std::make_unique<Texture>("Retry", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upScore = std::make_unique<Texture>("Score:", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_GameOverScoreDest = Rectf(40.0f, m_WindowHeight * 0.55f, m_upScore->GetWidth(), m_upScore->GetHeight());
	m_upSaveScore = std::make_unique<Texture>("Save Score", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upSaveScoreHover = std::make_unique<Texture>("Save Score", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.75f, 0.0f, 0.0f, 1.0f));
	m_upSaveScoreDisabled = std::make_unique<Texture>("Save Score", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.5f, 0.5f, 0.5f, 1.0f));
	m_GameOverSaveScoreDest = Rectf(m_WindowWidth - 40.0f - m_upSaveScore->GetWidth() * 0.6f, m_WindowHeight * 0.05f, m_upSaveScore->GetWidth() * 0.6f, m_upSaveScore->GetHeight() * 0.6f);
	m_GameOverRetryDest = Rectf(40.0f, m_WindowHeight * 0.05f, m_upRetry->GetWidth() * 0.6f, m_upRetry->GetHeight() * 0.6f);
	m_GameOverMainMenuDest = Rectf(m_WindowWidth * 0.5f - m_upMainMenu->GetWidth() * 0.6f / 2.0f, m_WindowHeight * 0.05f, m_upMainMenu->GetWidth() * 0.6f, m_upMainMenu->GetHeight() * 0.6f);
	m_upSaved = std::make_unique<Texture>("  Saved  ", m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_upNoName = std::make_unique<Texture>("Enter name to save", m_FontString, (int)(m_WindowHeight * 0.09f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	m_GameOverHighScore1Pos = Point2f(m_WindowWidth * 0.30f, m_WindowHeight * 0.4f);
	m_HighscoreHighScore1Pos = Point2f(m_WindowWidth * 0.30f, m_WindowHeight * 0.63f);

	std::stringstream strstream;
	strstream << std::setw(10) << std::setfill(' ') << m_PlayerName;
	m_LongplayerName = strstream.str();
	m_upPlayerName = std::make_unique<Texture>(m_LongplayerName, m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));

	RefreshHighScores();
}

void Menu::DrawMainMenu() const
{
	switch (m_CurrentMenu)
	{
	case Main:
		{
			m_upTitle->Draw(Rectf((m_WindowWidth * 0.7f - m_upTitle->GetWidth()) / 2.0f, m_WindowHeight - m_upTitle->GetHeight() - m_WindowHeight * 0.067f, m_upTitle->GetWidth(), m_upTitle->GetHeight()));
			m_spShipsheet->Draw(Rectf((m_WindowWidth / 2.0f - 1.5f * m_ShipInfo.width) / 2.0f, 0.6f * m_WindowHeight - 1.5f * m_ShipInfo.height, 1.5f * m_ShipInfo.width, 1.5f * m_ShipInfo.height), Rectf(m_ShipInfo.offsetX, m_ShipInfo.offsetY + m_ShipInfo.height, m_ShipInfo.width, m_ShipInfo.height));
			if (utils::IsPointInRect(m_MousePos, m_ArrowLeftDest))
			{
				m_upArrowLeftHover->Draw(m_ArrowLeftDest);
			}
			else
			{
				m_upArrowLeft->Draw(m_ArrowLeftDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_ArrowRightDest))
			{
				m_upArrowRightHover->Draw(m_ArrowRightDest);
			}
			else
			{
				m_upArrowRight->Draw(m_ArrowRightDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_PlayDest))
			{
				m_upPlayHover->Draw(m_PlayDest);
			}
			else
			{
				m_upPlay->Draw(m_PlayDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_LearnToPlayDest))
			{
				m_upLearnToPlayHover->Draw(m_LearnToPlayDest);
			}
			else
			{
				m_upLearnToPlay->Draw(m_LearnToPlayDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_HighScoresDest))
			{
				m_upHighscoresHover->Draw(m_HighScoresDest);
			}
			else
			{
				m_upHighscores->Draw(m_HighScoresDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_QuitDest))
			{
				m_upQuitHover->Draw(m_QuitDest);
			}
			else
			{
				m_upQuit->Draw(m_QuitDest);
			}
			break;
		}		
	case LearnMovement:
		{
			m_upControlsTitle->Draw(Rectf((m_WindowWidth * 0.7f - m_upControlsTitle->GetWidth()) / 2.0f, m_WindowHeight - m_upControlsTitle->GetHeight() - m_WindowHeight * 0.1f, m_upControlsTitle->GetWidth(), m_upControlsTitle->GetHeight()));
			m_upControlsMovement->Draw();
			if (utils::IsPointInRect(m_MousePos, m_LearnArrowRightDest))
			{
				m_upArrowRightHover->Draw(m_LearnArrowRightDest);
			}
			else
			{
				m_upArrowRight->Draw(m_LearnArrowRightDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_MainMenuDest))
			{
				m_upMainMenuHover->Draw(m_MainMenuDest);
			}
			else
			{
				m_upMainMenu->Draw(m_MainMenuDest);
			}
			break;
		}
	case LearnFire:
		{
			m_upControlsTitle->Draw(Rectf((m_WindowWidth * 0.7f - m_upControlsTitle->GetWidth()) / 2.0f, m_WindowHeight - m_upControlsTitle->GetHeight() - m_WindowHeight * 0.1f, m_upControlsTitle->GetWidth(), m_upControlsTitle->GetHeight()));
			m_upControlsFire->Draw();
			if (utils::IsPointInRect(m_MousePos, m_LearnArrowRightDest))
			{
				m_upArrowRightHover->Draw(m_LearnArrowRightDest);
			}
			else
			{
				m_upArrowRight->Draw(m_LearnArrowRightDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_MainMenuDest))
			{
				m_upMainMenuHover->Draw(m_MainMenuDest);
			}
			else
			{
				m_upMainMenu->Draw(m_MainMenuDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_LearnArrowLeftDest))
			{
				m_upArrowLeftHover->Draw(m_LearnArrowLeftDest);
			}
			else
			{
				m_upArrowLeft->Draw(m_LearnArrowLeftDest);
			}
			break;
		}
	case LearnPickup:
		{
			m_upControlsTitle->Draw(Rectf((m_WindowWidth * 0.7f - m_upControlsTitle->GetWidth()) / 2.0f, m_WindowHeight - m_upControlsTitle->GetHeight() - m_WindowHeight * 0.1f, m_upControlsTitle->GetWidth(), m_upControlsTitle->GetHeight()));
			m_upControlsPickups->Draw();
			if (utils::IsPointInRect(m_MousePos, m_MainMenuDest))
			{
				m_upMainMenuHover->Draw(m_MainMenuDest);
			}
			else
			{
				m_upMainMenu->Draw(m_MainMenuDest);
			}
			if (utils::IsPointInRect(m_MousePos, m_LearnArrowLeftDest))
			{
				m_upArrowLeftHover->Draw(m_LearnArrowLeftDest);
			}
			else
			{
				m_upArrowLeft->Draw(m_LearnArrowLeftDest);
			}
			break;
		}
	case Highscores:
		{
			m_upHighscoresTitle->Draw(Rectf((m_WindowWidth * 0.7f - m_upHighscoresTitle->GetWidth()) / 2.0f, m_WindowHeight - m_upHighscoresTitle->GetHeight() - m_WindowHeight * 0.1f, m_upHighscoresTitle->GetWidth(), m_upHighscoresTitle->GetHeight()));
			if (utils::IsPointInRect(m_MousePos, m_MainMenuDest))
			{
				m_upMainMenuHover->Draw(m_MainMenuDest);
			}
			else
			{
				m_upMainMenu->Draw(m_MainMenuDest);
			}
			for (size_t i = m_HighscorePage * 6; i < (m_upHighScoreTextures.size() < (m_HighscorePage + 1) * 6 ? m_upHighScoreTextures.size() : (m_HighscorePage + 1) * 6); ++i)
			{
				m_upHighScoreTextures[i]->Draw(Point2f(m_HighscoreHighScore1Pos.x, m_HighscoreHighScore1Pos.y - i % 6 * m_WindowHeight * 0.1f));
			}

			if (m_HighscorePage != m_NumHighScorePages)
			{
				if (utils::IsPointInRect(m_MousePos, m_LearnArrowRightDest))
				{
					m_upArrowRightHover->Draw(m_LearnArrowRightDest);
				}
				else
				{
					m_upArrowRight->Draw(m_LearnArrowRightDest);
				}
			}
			if (m_HighscorePage != 0)
			{
				if (utils::IsPointInRect(m_MousePos, m_LearnArrowLeftDest))
				{
					m_upArrowLeftHover->Draw(m_LearnArrowLeftDest);
				}
				else
				{
					m_upArrowLeft->Draw(m_LearnArrowLeftDest);
				}
			}
			break;
		}
	case GameOver:
	{
		m_upGameOverTitle->Draw(Rectf((m_WindowWidth * 0.7f - m_upTitle->GetWidth()) / 2.0f, m_WindowHeight - m_upTitle->GetHeight() - m_WindowHeight * 0.067f, m_upTitle->GetWidth(), m_upTitle->GetHeight()));
		if (utils::IsPointInRect(m_MousePos, m_GameOverMainMenuDest))
		{
			m_upMainMenuHover->Draw(m_GameOverMainMenuDest);
		}
		else
		{
			m_upMainMenu->Draw(m_GameOverMainMenuDest);
		}
		if (utils::IsPointInRect(m_MousePos, m_GameOverRetryDest))
		{
			m_upRetryHover->Draw(m_GameOverRetryDest);
		}
		else
		{
			m_upRetry->Draw(m_GameOverRetryDest);
		}
		if (m_Saved)
		{
			m_upSaved->Draw(m_GameOverSaveScoreDest);
		}
		else if (m_PlayerName == "")
		{
			m_upSaveScoreDisabled->Draw(m_GameOverSaveScoreDest);
		}
		else if (utils::IsPointInRect(m_MousePos, m_GameOverSaveScoreDest))
		{
			m_upSaveScoreHover->Draw(m_GameOverSaveScoreDest);
		}
		else
		{
			m_upSaveScore->Draw(m_GameOverSaveScoreDest);
		}
		m_upScore->Draw(m_GameOverScoreDest);
		if (m_upObtainedScore != nullptr) m_upObtainedScore->Draw(Point2f(m_WindowWidth * 0.25f, m_GameOverScoreDest.bottom - m_GameOverScoreDest.height * 0.12f));
		if (m_PlayerName.length() != 0)
		{
			m_upPlayerName->Draw(Point2f(m_WindowWidth * 0.45f, m_GameOverScoreDest.bottom));
		}
		else
		{
			m_upNoName->Draw(Point2f(m_WindowWidth * 0.45f, m_GameOverScoreDest.bottom));
		}
		for (size_t i = 0; i < (m_upHighScoreTextures.size() < 3 ? m_upHighScoreTextures.size() : 3); ++i)
		{
			m_upHighScoreTextures[i]->Draw(Point2f(m_GameOverHighScore1Pos.x, m_GameOverHighScore1Pos.y - i * m_WindowHeight * 0.1f));
		}
	}
	default:
		break;
	}
}

void Menu::ColorUp()
{
	switch (m_ActiveColor)
	{
	case green:
		m_ActiveColor = Color::red;
		break;
	case red:
		m_ActiveColor = Color::blue;
		break;
	case blue:
		m_ActiveColor = Color::yellow;
		break;
	case yellow:
		m_ActiveColor = Color::green;
		break;
	case gray:
		m_ActiveColor = Color::green;
		break;
	default:
		break;
	}
	m_ShipInfo = SpriteReader::GetInstance().GetFullShip(m_ActiveColor);
}

void Menu::ColorDown()
{
	switch (m_ActiveColor)
	{
	case green:
		m_ActiveColor = Color::yellow;
		break;
	case red:
		m_ActiveColor = Color::green;
		break;
	case blue:
		m_ActiveColor = Color::red;
		break;
	case yellow:
		m_ActiveColor = Color::blue;
		break;
	case gray:
		m_ActiveColor = Color::yellow;
		break;
	default:
		break;
	}
	m_ShipInfo = SpriteReader::GetInstance().GetFullShip(m_ActiveColor);
}

void Menu::Update(Point2f mousePos)
{
	m_MousePos = mousePos;
	if (m_CurrentMenu == ActiveMenu::GameOver)
	{
		//get input
		const Uint8 *pKeysStates = SDL_GetKeyboardState(nullptr);
		if (pKeysStates[SDL_SCANCODE_A])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('a');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_B])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('b');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_C])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('c');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_D])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('d');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_E])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('e');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_F])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('f');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_G])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('g');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_H])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('h');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_I])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('i');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_J])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('j');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_K])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('k');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_L])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('l');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_M])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('m');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_N])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('n');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_O])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('o');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_P])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('p');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_Q])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('q');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_R])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('r');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_S])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('s');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_T])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('t');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_U])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('u');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_V])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('v');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_W])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('w');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_X])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('x');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_Y])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('y');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_Z])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back('z');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_SPACE])
		{
			if (m_ReadLetter) return;
			m_PlayerName.push_back(' ');
			m_ReadLetter = true;
		}
		else if (pKeysStates[SDL_SCANCODE_BACKSPACE])
		{
			if (m_ReadLetter) return;
			if (m_PlayerName.length() == 0) return;
			m_PlayerName.pop_back();
			m_ReadLetter = true;
		}
		else
		{
			m_ReadLetter = false;
		}
		if (m_PlayerName.length() > 10) m_PlayerName = m_PlayerName.substr(0, 10);
		m_upObtainedScore = std::make_unique<Texture>(ScoreManager::GetInstance().GetScore(), m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
		if (m_PlayerName.length() != 0) m_upPlayerName = std::make_unique<Texture>(m_PlayerName, m_FontString, (int)(m_WindowHeight * 0.1f), Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	}
}

std::pair<MenuActions, Color> Menu::OnClick()
{
	switch (m_CurrentMenu)
	{
	case Main:
		if (utils::IsPointInRect(m_MousePos, m_ArrowLeftDest))
		{
			m_ClickSound.Play(0);
			ColorDown();
		}
		if (utils::IsPointInRect(m_MousePos, m_ArrowRightDest))
		{
			m_ClickSound.Play(0);
			ColorUp();
		}
		if (utils::IsPointInRect(m_MousePos, m_LearnToPlayDest))
		{
			m_ClickSound.Play(0);
			m_CurrentMenu = ActiveMenu::LearnMovement;
		}
		if (utils::IsPointInRect(m_MousePos, m_HighScoresDest))
		{
			m_ClickSound.Play(0);
			m_CurrentMenu = ActiveMenu::Highscores;
		}
		if (utils::IsPointInRect(m_MousePos, m_PlayDest))
		{
			m_ClickSound.Play(0);
			return std::make_pair(MenuActions::Play, m_ActiveColor);
		}
		if (utils::IsPointInRect(m_MousePos, m_QuitDest))
		{
			m_ClickSound.Play(0);
			return std::make_pair(MenuActions::Quit, m_ActiveColor);
		}
		break;
	case LearnMovement:
		if (utils::IsPointInRect(m_MousePos, m_MainMenuDest))
		{
			m_CurrentMenu = ActiveMenu::Main;
			m_ClickSound.Play(0);
		}
		if (utils::IsPointInRect(m_MousePos, m_LearnArrowRightDest))
		{
			m_ClickSound.Play(0);
			m_CurrentMenu = ActiveMenu::LearnFire;
		}
		break;
	case LearnFire:
		if (utils::IsPointInRect(m_MousePos, m_MainMenuDest))
		{
			m_ClickSound.Play(0);
			m_CurrentMenu = ActiveMenu::Main;
		}
		if (utils::IsPointInRect(m_MousePos, m_LearnArrowRightDest))
		{
			m_ClickSound.Play(0);
			m_CurrentMenu = ActiveMenu::LearnPickup;
		}
		if (utils::IsPointInRect(m_MousePos, m_LearnArrowLeftDest))
		{
			m_ClickSound.Play(0);
			m_CurrentMenu = ActiveMenu::LearnMovement;
		}
		break;
	case LearnPickup:
		if (utils::IsPointInRect(m_MousePos, m_MainMenuDest))
		{
			m_ClickSound.Play(0);
			m_CurrentMenu = ActiveMenu::Main;
		}
		if (utils::IsPointInRect(m_MousePos, m_LearnArrowLeftDest))
		{
			m_ClickSound.Play(0);
			m_CurrentMenu = ActiveMenu::LearnFire;
		}
		break;
	case Highscores:
		if (utils::IsPointInRect(m_MousePos, m_MainMenuDest))
		{
			m_ClickSound.Play(0);
			m_HighscorePage = 0;
			m_CurrentMenu = ActiveMenu::Main;
		}
		if (m_HighscorePage != m_NumHighScorePages)
		{
			if (utils::IsPointInRect(m_MousePos, m_LearnArrowRightDest))
			{
				m_ClickSound.Play(0);
				++m_HighscorePage;
			}
		}
		if (m_HighscorePage != 0)
		{
			if (utils::IsPointInRect(m_MousePos, m_LearnArrowLeftDest))
			{
				m_ClickSound.Play(0);
				--m_HighscorePage;
			}
		}
		break;
	case GameOver:
		if (utils::IsPointInRect(m_MousePos, m_GameOverMainMenuDest))
		{
			m_ClickSound.Play(0);
			m_Saved = false;
			m_CurrentMenu = ActiveMenu::Main;
		}
		if (utils::IsPointInRect(m_MousePos, m_GameOverRetryDest))
		{
			m_ClickSound.Play(0);
			m_Saved = false;
			return std::make_pair(MenuActions::Play, m_ActiveColor);
		}
		if (!m_Saved && m_PlayerName != "" && utils::IsPointInRect(m_MousePos, m_GameOverSaveScoreDest))
		{
			m_ClickSound.Play(0);
			SaveScore();
		}
		break;
	default:
		break;
	}
	return std::make_pair(MenuActions::Nothing, Color::green);
}

void Menu::SetActiveMenu(ActiveMenu a)
{
	m_CurrentMenu = a;
}

void Menu::SaveScore()
{
	ScoreManager::GetInstance().SaveScore(m_PlayerName);
	m_Saved = true;
	RefreshHighScores();
}

void Menu::RefreshHighScores()
{
	std::multimap<int, std::string> scores = ScoreManager::GetInstance().GetHighScores();
	size_t counter = 1;
	std::multimap<int, std::string>::reverse_iterator it = scores.rbegin();
	m_upHighScoreTextures.clear();
	while (counter <= scores.size())
	{
		std::stringstream strstr;
		strstr << counter << ".   " << std::setw(6) << std::setfill('0') << (it)->first << "   " << (it)->second;
		std::string entry = strstr.str();
		m_upHighScoreTextures.push_back(std::make_unique<Texture>(entry, m_FontString, (int)(m_WindowHeight * 0.08f), Color4f(0.0f, 0.0f, 0.0f, 1.0f)));
		it++;
		counter++;
	}
	m_NumHighScorePages = (int)((scores.size() - 1) / 6.0f);
}
