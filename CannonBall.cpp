//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "CannonBall.h"
#include "ScoreManager.h"


CannonBall::CannonBall(Point2f pos, Vector2f v, std::weak_ptr<Texture> spTex, float width, float height, float offsetX, float offsetY)
	: m_Position{ pos }
	, m_Velocity{ v }
	, m_StartPosition{ pos }
	, m_wpTex{ spTex }
	, m_Width{ width }
	, m_Height{ height }
	, m_OffsetX{ offsetX }
	, m_OffsetY{ offsetY }
	, m_Alive{ true }
{
}

void CannonBall::Update(float elapsedSec, std::vector<Island>& islands, std::vector<std::shared_ptr<EnemyShip>>& enemyShips)
{
	utils::HitInfo hit;
	Point2f dest = Point2f(m_Position.x + m_Velocity.x * elapsedSec, m_Position.y + m_Velocity.y * elapsedSec);

	for (std::shared_ptr<EnemyShip>& enemy : enemyShips)
	{
		if (m_Alive)
		{
			utils::Raycast(enemy->GetCollision(), m_Position, dest, hit);
			if (hit.intersectPoint.x != 0)
			{
				m_Alive = false;
				m_Position = hit.intersectPoint;
				enemy->IsHit(30);
				ScoreManager::GetInstance().AddPoints(10);
			}
		}
	}

	if (m_Alive)
	{
		for (size_t i = 0; i < islands.size(); ++i)
		{
			for (size_t j = 0; j < islands[i].GetCannons().size(); ++j)
			{
				if (m_Alive)
				{
					utils::Raycast(islands[i].GetCannons()[j].GetCollision(), m_Position, dest, hit);
					if (hit.intersectPoint.x != 0)
					{
						m_Alive = false;
						m_Position = hit.intersectPoint;
						islands[i].GetCannons()[j].Die();
						islands[i].EraseCannon(j);
						ScoreManager::GetInstance().AddPoints(50);
					}
				}
			}
			if (m_Alive)
			{
				utils::Raycast(islands[i].GetFortVertices(), m_Position, dest, hit);
				if (hit.intersectPoint.x != 0)
				{
					m_Alive = false;
					m_Position = hit.intersectPoint;
				}
			}
		}
	}
	if (m_Alive)
	{
		m_Position.x += m_Velocity.x * elapsedSec;
		m_Position.y += m_Velocity.y * elapsedSec;
	}	
}

void CannonBall::Update(float elapsedSec, Ship &ship)
{
	utils::HitInfo hit;
	Point2f dest = Point2f(m_Position.x + m_Velocity.x * elapsedSec, m_Position.y + m_Velocity.y * elapsedSec);
	if (m_Alive)
	{
		utils::Raycast(ship.GetCollision(), m_Position, dest, hit);
		if (hit.intersectPoint.x != 0)
		{
			m_Alive = false;
			m_Position = hit.intersectPoint;
			ship.IsHit(10.0f);
		}
	}
	if (m_Alive)
	{
		m_Position.x += m_Velocity.x * elapsedSec;
		m_Position.y += m_Velocity.y * elapsedSec;
	}
}

void CannonBall::Update(float elapsedSec, Ship & ship, std::vector<Island>& islands)
{
	utils::HitInfo hit;
	Point2f dest = Point2f(m_Position.x + m_Velocity.x * elapsedSec, m_Position.y + m_Velocity.y * elapsedSec);
	if (m_Alive)
	{
		utils::Raycast(ship.GetCollision(), m_Position, dest, hit);
		if (hit.intersectPoint.x != 0)
		{
			m_Alive = false;
			m_Position = hit.intersectPoint;
			ship.IsHit(10.0f);
		}
	}
	for (size_t i = 0; i < islands.size(); ++i)
	{
		if (m_Alive)
		{
			utils::Raycast(islands[i].GetFortVertices(), m_Position, dest, hit);
			if (hit.intersectPoint.x != 0)
			{
				m_Alive = false;
				m_Position = hit.intersectPoint;
			}
		}
	}
	if (m_Alive)
	{
		m_Position.x += m_Velocity.x * elapsedSec;
		m_Position.y += m_Velocity.y * elapsedSec;
	}
}

void CannonBall::Draw() const
{
	glPushMatrix();
	glTranslatef(m_Position.x, m_Position.y, 0.0f);
	m_wpTex.lock()->Draw(Rectf(0.0f - m_Width / 2, 0.0f - m_Height / 2, m_Width, m_Height), Rectf(m_OffsetX, m_OffsetY + m_Height, m_Width, m_Height));
	glPopMatrix();
}

float CannonBall::GetDisplacement() const
{
	return Vector2f(m_Position - m_StartPosition).Length();
}

Point2f CannonBall::GetPosition() const
{
	return m_Position;
}

bool CannonBall::IsAlive() const
{
	return m_Alive;
}
