//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "Game.h"
#include <iostream>
#include "SVGParser.h"



Game::Game( const Window& window ) 
	: m_Window{ window }
	, m_MaxTime{ 180.0f }
	, m_Ship{ 1, Color::red }
	, m_Camera{ 800.0f, 600.0f, m_CameraScale }
	, m_CameraScale{ 0.55f }
	, m_OriginalCameraScale{ m_CameraScale }
	, m_Music { "resources/sounds/music.mp3" }
	, m_Hud{ Point2f(10.0f, m_Window.height - 50.0f), 300.0f, 40.0f, Color::red, m_Ship.GetHealth(),
		Point2f(m_Window.width - 310.0f, m_Window.height - 50.0f), 300.0f, 40.0f, m_Ship.GetSpeed(),
		Point2f(10.0f, 10.0f), 40.0f, 300.0f, m_Ship.GetPickupTimer(),  m_Ship.GetCurrentPickup(),
		Point2f(m_Window.width - 50.0f, 10.0f), 40.0f, 300.0f, m_MaxTime, m_TimeLimit}
	, m_spEnemyShips{}
	, m_MenuBackground{ "resources/images/Background.png" }
	, m_State{ GameState::InMenu }
	, m_Menu{ m_Window.width, m_Window.height }
	, m_TimeLimit{ 180.0f }
	, m_MenuMusic{ "resources/sounds/MenuMusic.mp3" }
{
	Initialize( );
}

Game::~Game( )
{
	Cleanup( );
}

void Game::Initialize( )
{
	m_MenuMusic.Play(true);
	if (m_State != GameState::GamePlay) return;
	StartGame();
}

void Game::Cleanup( )
{
	delete &CannonBallManager::GetInstance();
	delete &SpriteReader::GetInstance();
	delete &ImpactEffectManager::GetInstance();
	delete &ScoreManager::GetInstance();
}

void Game::Update( float elapsedSec )
{
	switch (m_State)
	{
	case GamePlay:
		{
			if (!m_Ship.IsAlive()) 
			{
				m_MenuMusic.Play(true);
				m_State = GameState::InMenu;
				m_Menu.SetActiveMenu(ActiveMenu::GameOver);
				return;
			}			
			m_Level.Update(elapsedSec, m_Ship.GetShipCenter());
			m_Ship.Update(m_Level.GetWind(), elapsedSec, m_Level);
			m_PickupManager.Update(m_Ship);
			for (std::shared_ptr<EnemyShip> enemy : m_spEnemyShips)
			{
				enemy->Update(m_Level.GetWind(), elapsedSec, m_Level, m_Ship.GetShipCenter(), m_spEnemyShips);
			}
			CannonBallManager::GetInstance().Update(elapsedSec, m_Level.GetIslands(), m_Ship, m_spEnemyShips);
			//clear defeated ships
			for (size_t i = 0; i < m_spEnemyShips.size(); ++i)
			{
				if (!m_spEnemyShips[i]->IsAlive())
				{
					m_spEnemyShips.erase(m_spEnemyShips.begin() + i);
					ScoreManager::GetInstance().AddPoints(70);
				}
			}

			//zoom
			Rectf ballBounds = CannonBallManager::GetInstance().GetBallBounds();
			Rectf levelBounds = m_Level.GetBoundaries();
			if (ballBounds.left < levelBounds.left + 100.0f)
				ballBounds.left = levelBounds.left + 100.0f;
			if (ballBounds.bottom < levelBounds.bottom + 100.0f)
				ballBounds.bottom = levelBounds.bottom + 100.0f;
			if (ballBounds.left + ballBounds.width > levelBounds.left + levelBounds.width - 100.0f)
				ballBounds.width = levelBounds.left + levelBounds.width - 100.0f - ballBounds.left;
			if (ballBounds.bottom + ballBounds.height > levelBounds.bottom + levelBounds.height - 100.0f)
				ballBounds.height = levelBounds.bottom + levelBounds.height - 100.0f - ballBounds.bottom;

			if (utils::IsIntersecting(ballBounds, m_Camera.GetSafeBox()))
			{
				m_CameraScale -= m_ZoomSpeed * elapsedSec;
			}
			else
			{
				m_CameraScale += m_ZoomSpeed * elapsedSec / 2.0f;
				if (m_CameraScale > m_OriginalCameraScale) m_CameraScale = m_OriginalCameraScale;
			}
			m_TimeLimit -= elapsedSec;
			if (m_TimeLimit < 0) TimesUp();
		}
		break;
	case InMenu:
		m_Menu.Update(m_MousePos);
		break;
	case GameOver:
		break;
	default:
		break;
	}

	
}

void Game::Draw( )
{
	switch (m_State)
	{
	case GamePlay:
		{
			ClearBackground();
			glPushMatrix();
			glScalef(m_CameraScale, m_CameraScale, 1.0f);
			glTranslatef(-m_Camera.GetPosition(m_Ship.GetShipCenter()).x, -m_Camera.GetPosition(m_Ship.GetShipCenter()).y, 0.0f);

			m_Level.Draw();
			m_Ship.DrawTrail();
			for (std::shared_ptr<EnemyShip> enemy : m_spEnemyShips)
			{
				enemy->DrawTrail();
			}
			m_PickupManager.Draw();
			m_Ship.Draw();
			for (std::shared_ptr<EnemyShip> enemy : m_spEnemyShips)
			{
				enemy->Draw();
			}
			CannonBallManager::GetInstance().Draw();
			utils::SetColor(Color4f(0.0f, 1.0f, 0.0f, 1.0f));

			glPopMatrix();
			m_Hud.DrawHealthBar();
			m_Hud.DrawSpeedBar();
			m_Hud.DrawPickup();
			m_Hud.DrawTimer();
			if (!m_Ship.IsAlive())
			{
				utils::SetColor(Color4f(0.0f, 0.0f, 0.0f, 0.3f));
				utils::FillRect(0.0f, 0.0f, m_Window.width, m_Window.height);
			}
			ScoreManager::GetInstance().Draw();
		}
		break;
	case InMenu:
		ClearBackground();
		m_MenuBackground.Draw(Rectf(0.0f, 0.0f, m_Window.width, m_Window.height));
		m_Menu.DrawMainMenu();
		break;
	case GameOver:
		break;
	default:
		break;
	}
	
}

void Game::StartGame()
{
	SpriteInfo info = SpriteReader::GetInstance().GetCannonBallInfo();
	CannonBallManager::GetInstance().SetBallData(m_Ship.GetTexture(), info.width, info.height, info.offsetX, info.offsetY);
	CannonBallManager::GetInstance().ClearAll();
	m_Ship.SetPosition(Point2f(1300.0f, 196.0f));
	m_Ship.Reset();
	m_Camera.SetBoundaries(m_Level.GetBoundaries());
	m_Level.Reset();
	ScoreManager::GetInstance().SetWindowWidth(m_Window.width);
	ScoreManager::GetInstance().SetWindowHeight(m_Window.height);
	ScoreManager::GetInstance().Reset();
	ImpactEffectManager::GetInstance().Clear();
	m_TimeLimit = m_MaxTime;

	m_spEnemyShips.clear();
	{
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));
		m_spEnemyShips.push_back(std::make_shared<EnemyShip>(EnemyShip(1)));

		m_spEnemyShips[0]->SetPosition(Point2f(225.0f, 1396.0f));
		m_spEnemyShips[1]->SetPosition(Point2f(225.0f, 2496.0f));
		m_spEnemyShips[2]->SetPosition(Point2f(225.0f, 2866.0f));
		m_spEnemyShips[3]->SetPosition(Point2f(1350.0f, 3696.0f));
		m_spEnemyShips[4]->SetPosition(Point2f(1650.0f, 3696.0f));
		m_spEnemyShips[5]->SetPosition(Point2f(1950.0f, 3696.0f));
		m_spEnemyShips[6]->SetPosition(Point2f(1500.0f, 3446.0f));
		m_spEnemyShips[7]->SetPosition(Point2f(1800.0f, 3446.0f));
		m_spEnemyShips[8]->SetPosition(Point2f(1390.0f, 2516.0f));
		m_spEnemyShips[9]->SetPosition(Point2f(1480.0f, 2296.0f));
		m_spEnemyShips[10]->SetPosition(Point2f(2870.0f, 706.0f));
		m_spEnemyShips[11]->SetPosition(Point2f(2870.0f, 426.0f));
		m_spEnemyShips[12]->SetPosition(Point2f(2450.0f, 1696.0f));
		m_spEnemyShips[13]->SetPosition(Point2f(2750.0f, 1696.0f));
		m_spEnemyShips[14]->SetPosition(Point2f(3400.0f, 2796.0f));
		m_spEnemyShips[15]->SetPosition(Point2f(3600.0f, 2896.0f));
		m_spEnemyShips[16]->SetPosition(Point2f(3600.0f, 2596.0f));
		m_spEnemyShips[17]->SetPosition(Point2f(3400.0f, 1646.0f));
		m_spEnemyShips[18]->SetPosition(Point2f(3600.0f, 1746.0f));
		m_spEnemyShips[19]->SetPosition(Point2f(3600.0f, 1446.0f));
	}
	
	m_PickupManager.Reset();
	{
		m_PickupManager.AddPickup(PickupType::Heal, Point2f(350.0f, 350.0f));
		m_PickupManager.AddPickup(PickupType::Heal, Point2f(350.0f, 3746.0f));
		m_PickupManager.AddPickup(PickupType::Heal, Point2f(3746.0f, 350.0f));
		m_PickupManager.AddPickup(PickupType::Heal, Point2f(3746.0f, 3746.0f));
		m_PickupManager.AddPickup(PickupType::Invincible, Point2f(2030.0f, 2336.0f));
		m_PickupManager.AddPickup(PickupType::Invincible, Point2f(2910.0f, 1403.0f));
		m_PickupManager.AddPickup(PickupType::Rapidfire, Point2f(980.0f, 1496.0f));
		m_PickupManager.AddPickup(PickupType::Rapidfire, Point2f(2390.0f, 3326.0f));
		m_PickupManager.AddPickup(PickupType::Speedboost, Point2f(660.0f, 2756.0f));
		m_PickupManager.AddPickup(PickupType::Speedboost, Point2f(1360.0f, 756.0f));
		m_PickupManager.AddPickup(PickupType::Speedboost, Point2f(3500.0f, 2246.0f));
	}

	m_Music.Play(true);
}

void Game::ProcessKeyDownEvent( const SDL_KeyboardEvent & e )
{
}

void Game::ProcessKeyUpEvent( const SDL_KeyboardEvent& e )
{
}

void Game::ProcessMouseMotionEvent( const SDL_MouseMotionEvent& e )
{
	if (m_State == GameState::GamePlay) return;
	m_MousePos = Point2f((float)e.x, (float)m_Window.height - e.y);
}

void Game::ProcessMouseDownEvent( const SDL_MouseButtonEvent& e )
{
	if (m_State == GameState::GamePlay) return;
	std::pair<MenuActions, Color> action = m_Menu.OnClick();
	switch (action.first)
	{
	case MenuActions::Play:
		m_Ship.SetColor(action.second);
		m_State = GameState::GamePlay;
		StartGame();
		break;
	case MenuActions::Quit:
		SDL_Event quitEvent;
		quitEvent.type = SDL_QUIT;
		SDL_PushEvent(&quitEvent);
		break;
	default:
		break;
	}
}

void Game::ProcessMouseUpEvent( const SDL_MouseButtonEvent& e )
{
}

void Game::ClearBackground( )
{
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );
}

void Game::TimesUp()
{
	m_MenuMusic.Play(true);
	m_Menu.SetActiveMenu(ActiveMenu::GameOver);
	m_State = GameState::InMenu;
}
