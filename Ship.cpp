//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "Ship.h"
#include "Matrix2x3.h"
#include <algorithm>


Ship::~Ship()
{
}

Ship::Ship(int damageState, Color c)
	: m_spTexture{ std::make_shared<Texture>("Resources/images/shipsMiscellaneous_sheet.png") }
	, m_SailAngle{ 20.0f }
	, m_Speed{ 0.0f }
	, m_DamageState{ damageState }
	, m_Color{ c }
	, m_DamageSound{ "resources/sounds/Damage.mp3" }
	, m_RepairSound{ "resources/sounds/Repair.mp3" }
	, m_OriginalBallSpawnPointsRight{ Point2f(-30.0f, -10.0f), Point2f(-30.0f, 3.0f), Point2f(-30.0f, 16.0f) }
	, m_OriginalBallSpawnPointsLeft{ Point2f(30.0f, -10.0f), Point2f(30.0f, 3.0f), Point2f(30.0f, 16.0f) }
	, m_OriginalCollisionPoly{ Point2f(-20.0f, -20.0f), Point2f(0.0f, -50.0f),  Point2f(20.0f, -20.0f),  Point2f(20.0f, 40.0f), Point2f(0.0f, 50.0f),  Point2f(-20.0f, 40.0f) }
	, m_ElapsedTime{ 0.0f }
	, m_ActivePickup{ PickupType::None }
	, m_PickupCountDown{ 0.0f }
	, m_Rotation{ 180.0f }
{
	if (damageState < 1 || damageState > 3)
	{
		std::cerr << "Invalid Damage stage, must be 1 2 or 3\n";
		return;
	}
	m_CollisionPoly = m_OriginalCollisionPoly;
	SetHull(SpriteReader::GetInstance().GetHull(damageState));
	SetSailSmall(SpriteReader::GetInstance().GetSailSmall(damageState, c));
	SetSailLarge(SpriteReader::GetInstance().GetSailLarge(damageState, c));
	SetNest(SpriteReader::GetInstance().GetNest());
	SetMast(SpriteReader::GetInstance().GetMastTop(c));
	SetCannons(SpriteReader::GetInstance().GetCannon(), 1);
}

void Ship::SetHull(ShipPart h)
{
	m_Hull = h;
}

void Ship::SetSailSmall(ShipPart ss)
{
	m_SailSmall = ss;
}

void Ship::SetSailLarge(ShipPart sl)
{
	m_SailLarge = sl;
}

void Ship::SetNest(ShipPart n)
{
	m_Nest = n;
}

void Ship::SetMast(ShipPart m)
{
	m_Mast = m;
}

void Ship::DrawTrail() const
{
	if (!m_MustDraw) return;
	for (size_t i = 0; i < m_Trail.size(); ++i)
	{
		utils::SetColor(Color4f(0.690196f, 0.913725f, 0.988235f, 1.0f));
		utils::FillEllipse(m_Trail[i].x + m_Hull.GetWidth() / 2, m_Trail[i].y + m_Hull.GetHeight() / 2, 30.0f * (1 + (float)i / 50.0f), 30.0f * (1 + (float)i / 50.0f));
	}

}

void Ship::Draw() const
{
	if (!m_MustDraw) return;

	//hull, bottom masts and cannons
	glPushMatrix();
	glTranslatef(m_Position.x + m_Hull.GetWidth() / 2, m_Position.y + m_Hull.GetHeight() / 2, 0.0f);
	glRotatef(m_Rotation, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_Hull.GetWidth() / 2, -m_Hull.GetHeight() / 2, 0.0f);
	m_spTexture->Draw(Rectf(0.0f, 0.0f, m_Hull.GetWidth(), m_Hull.GetHeight()), m_Hull.GetSrcRect());
	for (size_t i{0} ; i < m_LeftCannons.size() ; ++i)
	{
		m_spTexture->Draw(Rectf(31.0f, 38.0f + i * 13.0f, m_LeftCannons[i].GetWidth(), m_LeftCannons[i].GetHeight()), m_LeftCannons[i].GetSrcRect());
	}
	glPopMatrix();
	glPushMatrix();
	glTranslatef(m_Position.x + m_Hull.GetWidth() / 2, m_Position.y + m_Hull.GetHeight() / 2, 0.0f);
	glRotatef(m_Rotation + 180.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_Hull.GetWidth() / 2, -m_Hull.GetHeight() / 2, 0.0f);
	for (size_t i{ 0 }; i < m_RightCannons.size(); ++i)
	{
		m_spTexture->Draw(Rectf(31.0f, 32.0f + i * 13.0f, m_RightCannons[i].GetWidth(), m_RightCannons[i].GetHeight()), m_RightCannons[i].GetSrcRect());
	}
	glPopMatrix();
	
	//small sail
	glPushMatrix();
	glTranslatef(m_Position.x + m_Hull.GetWidth() / 2, m_Position.y + m_Hull.GetHeight() / 2, 0.0f);
	glRotatef(m_Rotation, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_Position.x - m_Hull.GetWidth() / 2, -m_Position.y - m_Hull.GetHeight() / 2, 0.0f);
	glTranslatef(m_Position.x + m_Hull.GetWidth() / 2, m_Position.y + m_Hull.GetHeight() * 0.23f, 0.0f);
	glRotatef(m_SailAngle, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_SailSmall.GetWidth() / 2, -m_SailSmall.GetHeight() * 0.75f, 1.0f);
	m_spTexture->Draw(Rectf(0.0f, 0.0f, m_SailSmall.GetWidth(), m_SailSmall.GetHeight()), m_SailSmall.GetSrcRect());
	glPopMatrix();

	//large sail
	glPushMatrix();
	glTranslatef(m_Position.x + m_Hull.GetWidth() / 2, m_Position.y + m_Hull.GetHeight() / 2, 0.0f);
	glRotatef(m_Rotation, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_Position.x - m_Hull.GetWidth() / 2,-m_Position.y - m_Hull.GetHeight() / 2, 0.0f);
	glTranslatef(m_Position.x + m_Hull.GetWidth() / 2, m_Position.y + m_Hull.GetHeight() * 0.80f, 0.0f);
	glRotatef(m_SailAngle, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_SailLarge.GetWidth() / 2, -m_SailLarge.GetHeight() * 0.90f, 1.0f);
	m_spTexture->Draw(Rectf(0.0f, 0.0f, m_SailLarge.GetWidth(), m_SailLarge.GetHeight()), m_SailLarge.GetSrcRect());
	glPopMatrix();

	//nest and top mast
	glPushMatrix();
	glTranslatef(m_Position.x + m_Hull.GetWidth() / 2, m_Position.y + m_Hull.GetHeight() / 2, 0.0f);
	glRotatef(m_Rotation, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_Hull.GetWidth() / 2, -m_Hull.GetHeight() / 2, 0.0f);
	m_spTexture->Draw(Rectf((m_Hull.GetWidth() - m_Nest.GetWidth()) / 2, m_Hull.GetHeight() * 0.78f, m_Nest.GetWidth(), m_Nest.GetHeight()), m_Nest.GetSrcRect());
	m_spTexture->Draw(Rectf((m_Hull.GetWidth() - m_Mast.GetWidth()) / 2, m_Hull.GetHeight() * 0.86f, m_Mast.GetWidth(), m_Mast.GetHeight()), m_Mast.GetSrcRect());	
	glPopMatrix();

	//range previews
	utils::SetColor(Color4f(1.0f, 0.0f, 0.0f, m_RangePreviewOpacity));
	if (m_ShowRangeRight) utils::FillPolygon(m_RangeRight);
	if (m_ShowRangeLeft) utils::FillPolygon(m_RangeLeft);
}

void Ship::SetRotation(float angle)
{
	m_Rotation = angle;
}

void Ship::SetSailRotation(float angle)
{
	m_SailAngle = angle;
	while (m_SailAngle > 360.0f) m_SailAngle -= 360.0f;
	while (m_SailAngle < -360.0f) m_SailAngle += 360.0f;
}

Point2f Ship::GetPosition() const
{
	return Point2f(m_Position.x + m_Hull.GetWidth()/2, m_Position.y + m_Hull.GetHeight()/2);
}

void Ship::SetPosition(Point2f pos)
{
	m_Position = pos;
}

void Ship::SetCannons(ShipPart c, int numCannons)
{
	m_LeftCannons.push_back(c);
	m_LeftCannons.push_back(c);
	m_LeftCannons.push_back(c);
	m_RightCannons.push_back(c);
	m_RightCannons.push_back(c);
	m_RightCannons.push_back(c);
}

void Ship::SetShowRangeRight(bool enable)
{
	m_ShowRangeRight = enable;
}

void Ship::SetShowRangeLeft(bool enable)
{
	m_ShowRangeLeft = enable;
}

void Ship::Update(Vector2f wind, float elapsedSec, const Level& level)
{
	if (!m_Dying)
	{
		Move(wind, elapsedSec);
		ShipCollisionInfo info;
		info.pos = m_Position;
		info.center = GetShipCenter();
		info.width = m_Hull.GetWidth();
		info.height = m_Hull.GetHeight();
		info.rotation = m_Rotation * 3.14159565f / 180.0f;

		Point2f collided = level.HandleCollision(info);
		if (collided != m_Position)
		{
			m_Speed = 0.0f;
			m_Position.x -= m_CurrentVelocity.x * elapsedSec * 5.0f;
			m_Position.y -= m_CurrentVelocity.y * elapsedSec * 5.0f;
			IsHit(20);
		}

		//opacity rangepreview
		m_ElapsedTime += elapsedSec;
		m_RangePreviewOpacity = std::abs(0.5f *std::cos(m_ElapsedTime)) + 0.25f;

		//cannon cooldown
		m_CooldownLeft += elapsedSec;
		m_CooldownRight += elapsedSec;

		//firing
		HandleFiring(elapsedSec);

		//update collision poly
		Matrix2x3 mt = Matrix2x3::CreateTranslationMatrix(GetShipCenter().x, GetShipCenter().y);
		Matrix2x3 mr = Matrix2x3::CreateRotationMatrix(m_Rotation);
		Matrix2x3 mw = mt * mr;
		for (size_t i = 0; i < m_CollisionPoly.size(); ++i)
		{
			m_CollisionPoly[i] = mw.Transform(m_OriginalCollisionPoly[i]);
		}

		// trail
		if (m_Trail.size() > 0)
		{
			if (std::sqrtf(std::powf((m_Trail.front().x - m_Position.x), 2) + std::powf((m_Trail.front().y - m_Position.y), 2)) > 5)
			{
				m_Trail.push_front(m_Position);
				if (m_Trail.size() > 50) m_Trail.pop_back();
			}
			else
			{
				m_Trail.push_front(Point2f(-5000.0f, -5000.0f));
				if (m_Trail.size() > 50) m_Trail.pop_back();
			}
		}
		else
		{
			m_Trail.push_front(m_Position);
		}

		// set poly for rangepreview
		m_RangeLeft = std::vector<Point2f>{ Point2f(30.0f, -15.0f), Point2f(30.0f, 20.0f), Point2f(530.0f, 20.0f), Point2f(530.0f, -15.0f) };
		m_RangeRight = std::vector<Point2f>{ Point2f(-30.0f, -15.0f), Point2f(-30.0f, 20.0f), Point2f(-530.0f, 20.0f), Point2f(-530.0f, -15.0f) };
		Matrix2x3 rpmt = Matrix2x3::CreateTranslationMatrix(GetShipCenter().x, GetShipCenter().y);
		Matrix2x3 rpmr = Matrix2x3::CreateRotationMatrix(m_Rotation);
		Matrix2x3 rpmw = rpmt * rpmr;
		for (size_t i = 0; i < m_RangeRight.size(); ++i)
		{
			m_RangeRight[i] = rpmw.Transform(m_RangeRight[i]);
			m_RangeLeft[i] = rpmw.Transform(m_RangeLeft[i]);
		}
	}
	else
	{
		if (m_DeathTimer < 1.0f)
		{
			if (m_DeathTimer > 0.5f) m_MustDraw = false;
			Point2f p;
			do
			{
				p = Point2f(GetShipCenter().x + rand() % (int)m_Hull.GetHeight() - m_Hull.GetHeight() /2, GetShipCenter().y + rand() % (int)m_Hull.GetHeight() - m_Hull.GetHeight() / 2);
			}
			while (!utils::IsPointInPolygon(p, GetCollision()));
			ImpactEffectManager::GetInstance().AddEffect(p, EffectType::boom);
		}
		else if (m_DeathTimer > 1.5f)
		{
			m_Alive = false;
		}
		m_DeathTimer += elapsedSec;
	}

	m_PickupCountDown -= elapsedSec;
	if (m_PickupCountDown < 0.0f && m_ActivePickup != PickupType::None)
	{
		switch (m_ActivePickup)
		{
		case Heal:
			break;
		case Speedboost:
			m_MaxSpeed = m_StandardMaxSpeed;
			break;
		case Rapidfire:
			m_CooldownTime = m_StandardCooldown;
			break;
		case Invincible:
			break;
		case None:
			break;
		default:
			break;
		}
		m_ActivePickup = PickupType::None;
	}
}

void Ship::Accellerate()
{
	m_Speed == 3.0f ? m_Speed = 3.0f : ++m_Speed;
	return;
}

void Ship::Decellerate()
{
	m_Speed == 0.0f ? m_Speed == 0.0f : --m_Speed;
	return;
}

void Ship::FireRight()
{
	if (m_CooldownRight < m_CooldownTime) return;
	m_BulletVelocityRight = (Vector2f(m_Direction.y, -m_Direction.x).Normalized() * m_CannonBallSpeed);
	m_BallSpawnPointsRight = m_OriginalBallSpawnPointsRight;
	Matrix2x3 mt = Matrix2x3::CreateTranslationMatrix(GetShipCenter().x, GetShipCenter().y);
	Matrix2x3 mr = Matrix2x3::CreateRotationMatrix(m_Rotation);
	Matrix2x3 mw = mt * mr;
	
	for (size_t i = 0; i < m_BallSpawnPointsRight.size(); ++i)
	{
		m_BallSpawnPointsRight[i] = mw.Transform(m_BallSpawnPointsRight[i]);
	}
	std::random_shuffle(m_BallSpawnPointsRight.begin(), m_BallSpawnPointsRight.end());
	m_CooldownRight = 0.0f;
	m_RightCannonShotsLeft = 3;
}

void Ship::FireLeft()
{
	if (m_CooldownLeft < m_CooldownTime) return;
	m_BulletVelocityLeft = (Vector2f(-m_Direction.y, m_Direction.x).Normalized() * m_CannonBallSpeed);
	m_BallSpawnPointsLeft = m_OriginalBallSpawnPointsLeft;
	Matrix2x3 mt = Matrix2x3::CreateTranslationMatrix(GetShipCenter().x, GetShipCenter().y);
	Matrix2x3 mr = Matrix2x3::CreateRotationMatrix(m_Rotation);
	Matrix2x3 mw = mt * mr;

	for (size_t i = 0; i < m_BallSpawnPointsLeft.size(); ++i)
	{
		m_BallSpawnPointsLeft[i] = mw.Transform(m_BallSpawnPointsLeft[i]);
	}
	std::random_shuffle(m_BallSpawnPointsLeft.begin(), m_BallSpawnPointsLeft.end());
	m_CooldownLeft = 0.0f;
	m_LeftCannonShotsLeft = 3;
}

void Ship::HandleFiring(float elapsedSec)
{
	m_RightCannonSoundTimer += elapsedSec;
	m_LeftCannonSoundTimer += elapsedSec;
	if (m_RightCannonShotsLeft > 0 && m_RightCannonSoundTimer > m_CannonSoundDelay)
	{
		--m_RightCannonShotsLeft;
		CannonBallManager::GetInstance().Fire(m_BallSpawnPointsRight[m_RightCannonShotsLeft], m_BulletVelocityRight, Source::Ships);
		m_RightCannonSoundTimer = 0.0f;
	}
	if (m_LeftCannonShotsLeft > 0 && m_LeftCannonSoundTimer > m_CannonSoundDelay)
	{
		--m_LeftCannonShotsLeft;
		CannonBallManager::GetInstance().Fire(m_BallSpawnPointsLeft[m_LeftCannonShotsLeft], m_BulletVelocityLeft, Source::Ships);
		m_LeftCannonSoundTimer = 0.0f;
	}
}

int Ship::TakeDamage()
{	
	if (m_DamageState < 3)
	{
		++m_DamageState;
		m_DamageSound.SetVolume(50);
		m_DamageSound.Play(0);
		SetHull(SpriteReader::GetInstance().GetHull(m_DamageState));
		SetSailSmall(SpriteReader::GetInstance().GetSailSmall(m_DamageState, m_Color));
		SetSailLarge(SpriteReader::GetInstance().GetSailLarge(m_DamageState, m_Color));
	}	
	return m_DamageState;
}

int Ship::Repair()
{
	if (m_DamageState > 1)
	{
		--m_DamageState;
		m_RepairSound.Play(0);
	}
	SetHull(SpriteReader::GetInstance().GetHull(m_DamageState));
	SetSailSmall(SpriteReader::GetInstance().GetSailSmall(m_DamageState, m_Color));
	SetSailLarge(SpriteReader::GetInstance().GetSailLarge(m_DamageState, m_Color));
	return m_DamageState;
}

void Ship::IsHit(float damage)
{
	if (m_ActivePickup == PickupType::Invincible) return;
	m_CurHealth -= damage;
	if (m_CurHealth < m_MaxHealth - (m_MaxHealth / 3) * m_DamageState)
	{
		TakeDamage();
	}
	if (m_CurHealth <= 0) m_Dying = true;
}

void Ship::IsHealed(float heal)
{
	m_CurHealth += heal;
	if (m_CurHealth > m_MaxHealth - (m_MaxHealth / 3) * m_DamageState)
	{
		Repair();
	}
	if (m_CurHealth > m_MaxHealth) m_CurHealth = m_MaxHealth;
}

void Ship::PickupPickedUp(PickupType p)
{
	if (m_ActivePickup == PickupType::Speedboost) m_MaxSpeed = m_StandardMaxSpeed;
	if (m_ActivePickup == PickupType::Rapidfire) m_CooldownTime = m_StandardCooldown;
	switch (p)
	{
	case Heal:
		IsHealed(40.0f);
		break;
	case Speedboost:
		m_MaxSpeed = m_BoostSpeed;
		m_PickupCountDown = 10.0f;
		m_ActivePickup = PickupType::Speedboost;
		break;
	case Rapidfire:
		m_CooldownTime = m_RapidCooldown;
		m_PickupCountDown = 10.0f;
		m_ActivePickup = PickupType::Rapidfire;
		break;
	case Invincible:
		m_PickupCountDown = 10.0f;
		m_ActivePickup = PickupType::Invincible;
		break;
	case None:
		break;
	default:
		break;
	}
}

int Ship::GetDamageState() const
{
	return m_DamageState;
}

void Ship::SetColor(Color c)
{
	m_Color = c;
	SetSailSmall(SpriteReader::GetInstance().GetSailSmall(m_DamageState, c));
	SetSailLarge(SpriteReader::GetInstance().GetSailLarge(m_DamageState, c));
	SetMast(SpriteReader::GetInstance().GetMastTop(c));
}

void Ship::SetAlive(bool value)
{
	m_Alive = value;
}

void Ship::Reset()
{
	m_Speed = 0.0f;
	m_RightCannonShotsLeft = 0;
	m_LeftCannonShotsLeft = 0;
	m_DamageState = 1;
	m_CurHealth = m_MaxHealth;
	m_MaxSpeed = m_StandardMaxSpeed;
	m_Alive = true;
	m_Dying = false;
	m_MustDraw = true;
	m_DeathTimer = 0.0f;
	SetHull(SpriteReader::GetInstance().GetHull(m_DamageState));
	SetSailSmall(SpriteReader::GetInstance().GetSailSmall(m_DamageState, m_Color));
	SetSailLarge(SpriteReader::GetInstance().GetSailLarge(m_DamageState, m_Color));
	m_Rotation = 180.0f;
}

Point2f Ship::GetShipCenter() const
{
	return Point2f(m_Position.x + m_Hull.GetWidth()/2, m_Position.y + m_Hull.GetHeight()/2);
}

std::shared_ptr<Texture> Ship::GetTexture() const
{
	return m_spTexture;
}

std::vector<Point2f> Ship::GetCollision() const
{
	return m_CollisionPoly;
}

float & Ship::GetHealth()
{
	return m_CurHealth;
}

bool Ship::IsAlive() const
{
	return m_Alive;
}

float & Ship::GetSpeed()
{
	return m_Speed;
}

PickupType & Ship::GetCurrentPickup()
{
	return m_ActivePickup;
}

float & Ship::GetPickupTimer()
{
	return m_PickupCountDown;
}

float Ship::GetWindAngle(Vector2f wind)
{
	return wind.AngleWith(m_Direction);
}

void Ship::Move(Vector2f wind, float elapsedSec)
{
	m_Direction = Vector2f(std::cos((m_Rotation - 90.0f) * 3.14159565f / 180.0f) * m_MaxSpeed, std::sin((m_Rotation - 90.0f) * 3.14159565f / 180.0f) * m_MaxSpeed);
	float angle = GetWindAngle(wind);
	float angleInDegrees = angle * 180.0f / 3.14159565f;
	if (angleInDegrees < 0.0f) angleInDegrees += 360.0f;
	float boatSpeed;

	if (angleInDegrees <= 90.0f)
	{
		//k(x) = -0.00309(x - 90)� + 100
		boatSpeed = -0.00309f * std::powf(angleInDegrees - 90, 2) + 100;
	}
	else if (angleInDegrees <= 135.0f)
	{
		//l(x) = -0.00741(x - 90)� + 100
		boatSpeed = -0.0074074f * std::powf(angleInDegrees - 90, 2) + 100;
	}
	else if (angleInDegrees <= 225.0f)
	{
		//m(x) = 0.02963(x - 180)� + 25
		boatSpeed = 0.02963f * std::powf(angleInDegrees - 180, 2) + 25;
	}
	else if (angleInDegrees <= 270.0f)
	{
		//n(x) = -0.00741(x - 270)� + 100
		boatSpeed = -0.0074074f * std::powf(angleInDegrees - 270, 2) + 100;
	}
	else
	{
		//o(x) = -0.00309(x - 270)� + 100
		boatSpeed = -0.00309f * std::powf(angleInDegrees - 270, 2) + 100;
	}
	boatSpeed /= 100;
	//set sail rotation
	if (angleInDegrees <= 55.0f) m_SailAngle = -angleInDegrees;
	else if (angleInDegrees < 160.0f) m_SailAngle = -55.0f;
	else if (angleInDegrees <= 200.0f) m_SailAngle = 0.0f;
	else if (angleInDegrees <= 305) m_SailAngle = 55.0f;
	else m_SailAngle = -angleInDegrees;

	//move ship
	m_CurrentVelocity.x = boatSpeed * m_Direction.x * m_Speed;
	m_CurrentVelocity.y = boatSpeed * m_Direction.y * m_Speed;
	m_Position.x += m_CurrentVelocity.x * elapsedSec;
	m_Position.y += m_CurrentVelocity.y * elapsedSec;
}
