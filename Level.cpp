//Arne Sercu
//1DAE17

#pragma once
#include "stdafx.h"
#include "Level.h"
#include "SVGParser.h"


Level::Level()
	: m_Wind{ 1.0f,0.0f }
	, m_Islands{}
{
	m_upSea = std::make_unique<Texture>("resources/images/sea.png");
	m_upMapBorder = std::make_unique<Texture>("resources/images/map_border.png");
	m_Boundaries = Rectf(0.0f, 0.0f, m_upMapBorder->GetWidth(), m_upMapBorder->GetHeight());
	m_Wind = m_Wind.Normalized();
	m_SeaRotation = (std::atan2f(m_Wind.y, m_Wind.x));
	SVGParser::GetVerticesFromSvgFile("resources/SVG/map_border.svg", m_BorderVertices);
	AddIslands();
	
}

void Level::Update(float elapsedsec, Point2f shipPos)
{
	UpdateSea(elapsedsec);
	for (size_t i = 0; i < m_Islands.size(); ++i)
	{
		m_Islands[i].Update(elapsedsec, shipPos);
	}
}

void Level::UpdateSea(float elapsedSec)
{
	m_SeaPosition.x += 80.0f * std::cosf(m_SeaRotation) * elapsedSec;
	m_SeaPosition.y += 80.0f * std::sinf(m_SeaRotation) * elapsedSec;

	if (m_SeaPosition.x > 90.0f * std::cosf(m_SeaRotation)) m_SeaPosition.x -= 90.0f * std::cosf(m_SeaRotation);
	if (m_SeaPosition.y > 90.0f * std::sinf(m_SeaRotation))	m_SeaPosition.y -= 90.0f * std::sinf(m_SeaRotation);
	if (m_SeaPosition.x < -90.0f * std::cosf(m_SeaRotation)) m_SeaPosition.x += 90.0f * std::cosf(m_SeaRotation);
	if (m_SeaPosition.y < -90.0f * std::sinf(m_SeaRotation)) m_SeaPosition.y += 90.0f * std::sinf(m_SeaRotation);
}

void Level::DrawSea() const
{
	glPushMatrix();
	glTranslatef(m_SeaPosition.x + m_upSea->GetWidth() / 2, m_SeaPosition.y + m_upSea->GetHeight() / 2, 0.0f);
	glRotatef(m_SeaRotation * 180.0f / 3.14159565f, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_upSea->GetWidth() / 2 - 30.0f, -m_upSea->GetHeight() / 2 - 30.0f, 0.0f);
	m_upSea->Draw();
	glPopMatrix();
}

void Level::AddIslands()
{
	m_Islands.push_back(Island(400.0f, 1846.0f, IslandType::type1));
	m_Islands.push_back(Island(1985.0f, 471.0f, IslandType::type1));
	m_Islands.push_back(Island(2890.0f, 3086.0f, IslandType::type1));
	m_Islands.push_back(Island(500.0f, 3066.0f, IslandType::type2));
	m_Islands.push_back(Island(1415.0f, 1406.0f, IslandType::type2));
	m_Islands.push_back(Island(1550.0f, 2696.0f, IslandType::type3));
	m_Islands.push_back(Island(3150.0f, 471.0f, IslandType::type3));
	m_Islands.push_back(Island(275.0f, 596.0f, IslandType::type4));
	m_Islands.push_back(Island(2500.0f, 1976.0f, IslandType::type4));
}

Point2f Level::HandleCollision(ShipCollisionInfo info) const
{
	Point2f returnvalue = info.pos;
	utils::HitInfo hitBack;
	utils::HitInfo hitFront;
	utils::HitInfo hitPort;
	utils::HitInfo hitStarboard;
	//front
	if (utils::Raycast(m_BorderVertices, info.center, Point2f(info.center.x + (info.height * std::sin(info.rotation) / 2), info.center.y - (info.height * std::cos(info.rotation) / 2)), hitFront))
	{
		returnvalue.x -= info.center.x + (info.height * std::sin(info.rotation) / 2) - hitFront.intersectPoint.x;
		returnvalue.y -= info.center.y - (info.height * std::cos(info.rotation) / 2) - hitFront.intersectPoint.y;
	}
	//back
	if (utils::Raycast(m_BorderVertices, info.center, Point2f(info.center.x - (info.height * std::sin(info.rotation) / 2), info.center.y + (info.height * std::cos(info.rotation) / 2)), hitBack))
	{
		returnvalue.x -= info.center.x - (info.height * std::sin(info.rotation) / 2) - hitBack.intersectPoint.x;
		returnvalue.y -= info.center.y + (info.height * std::cos(info.rotation) / 2) - hitBack.intersectPoint.y;
	}
	//port
	if (utils::Raycast(m_BorderVertices, info.center, Point2f(info.center.x + (info.width * std::cos(info.rotation) / 2), info.center.y + (info.width * std::sin(info.rotation) / 2)), hitPort))
	{
		returnvalue.x -= info.center.x + (info.width * std::cos(info.rotation) / 2) - hitPort.intersectPoint.x;
		returnvalue.y -= info.center.y + (info.width * std::sin(info.rotation) / 2) - hitPort.intersectPoint.y;
	}
	//starboard
	if (utils::Raycast(m_BorderVertices, info.center, Point2f(info.center.x - (info.width * std::cos(info.rotation) / 2), info.center.y - (info.width * std::sin(info.rotation) / 2)), hitStarboard))
	{
		returnvalue.x -= info.center.x - (info.width * std::cos(info.rotation) / 2) - hitStarboard.intersectPoint.x;
		returnvalue.y -= info.center.y - (info.width * std::sin(info.rotation) / 2) - hitStarboard.intersectPoint.y;
	}

	// islands
	for (size_t i = 0 ; i < m_Islands.size(); ++i)
	{
		std::vector<Point2f> vert = m_Islands[i].GetVertices();

		//front
		if (utils::Raycast(vert, info.center, Point2f(info.center.x + (info.height * std::sin(info.rotation) / 2), info.center.y - (info.height * std::cos(info.rotation) / 2)), hitFront))
		{
			returnvalue.x -= info.center.x + (info.height * std::sin(info.rotation) / 2) - hitFront.intersectPoint.x;
			returnvalue.y -= info.center.y - (info.height * std::cos(info.rotation) / 2) - hitFront.intersectPoint.y;
		}
		//back
		if (utils::Raycast(vert, info.center, Point2f(info.center.x - (info.height * std::sin(info.rotation) / 2), info.center.y + (info.height * std::cos(info.rotation) / 2)), hitBack))
		{
			returnvalue.x -= info.center.x - (info.height * std::sin(info.rotation) / 2) - hitBack.intersectPoint.x;
			returnvalue.y -= info.center.y + (info.height * std::cos(info.rotation) / 2) - hitBack.intersectPoint.y;
		}
		//port
		if (utils::Raycast(vert, info.center, Point2f(info.center.x + (info.width * std::cos(info.rotation) / 2), info.center.y + (info.width * std::sin(info.rotation) / 2)), hitPort))
		{
			returnvalue.x -= info.center.x + (info.width * std::cos(info.rotation) / 2) - hitPort.intersectPoint.x;
			returnvalue.y -= info.center.y + (info.width * std::sin(info.rotation) / 2) - hitPort.intersectPoint.y;
		}
		//starboard
		if (utils::Raycast(vert, info.center, Point2f(info.center.x - (info.width * std::cos(info.rotation) / 2), info.center.y - (info.width * std::sin(info.rotation) / 2)), hitStarboard))
		{
			returnvalue.x -= info.center.x - (info.width * std::cos(info.rotation) / 2) - hitStarboard.intersectPoint.x;
			returnvalue.y -= info.center.y - (info.width * std::sin(info.rotation) / 2) - hitStarboard.intersectPoint.y;
		}
	}
	return returnvalue;
}

Rectf Level::GetBoundaries() const
{
	return m_Boundaries;
}

Vector2f Level::GetWind() const
{
	return m_Wind;
}

void Level::Draw() const
{
	DrawSea();
	m_upMapBorder->Draw();
	for (size_t i = 0; i < m_Islands.size(); ++i)
	{
		m_Islands[i].Draw();
	}
}

void Level::Reset()
{
	m_Islands.clear();
	AddIslands();
}

std::vector<Island>& Level::GetIslands()
{
	return m_Islands;
}
