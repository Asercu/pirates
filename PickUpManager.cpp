//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "PickUpManager.h"


PickUpManager::PickUpManager()
	: m_Pickups{}
{
	m_upTex = std::make_unique<Texture>("resources/images/Pickups.png");
}

void PickUpManager::AddPickup(PickupType t, Point2f p)
{
	Pickup toAdd;
	toAdd.position = p;
	toAdd.type = t;
	m_Pickups.push_back(toAdd);
}

void PickUpManager::Update(Ship& player)
{
	for (size_t i = 0; i < m_Pickups.size(); ++i)
	{
		if (utils::IsOverlapping(player.GetCollision(), Circlef(m_Pickups[i].position, 32.0f)))
		{
			player.PickupPickedUp(m_Pickups[i].type);
			m_Pickups.erase(m_Pickups.begin() + i);
		}
	}
}

void PickUpManager::Draw() const
{
	for (const Pickup& p : m_Pickups)
	{
		m_upTex->Draw(Rectf(p.position.x - 32.0f, p.position.y - 32.0f, 64.0f, 64.0f), Rectf(128.0f * p.type, 0.0f, 128.0f, 128.0f));
	}
}

void PickUpManager::Reset()
{
	m_Pickups.clear();
}
