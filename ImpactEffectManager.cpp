//Arne Sercu
//1DAE17

#pragma once
#include "stdafx.h"
#include "ImpactEffectManager.h"


ImpactEffectManager::ImpactEffectManager()
	: m_Effects{}
	, m_FrameTime{ 0.05f }
{
	m_upEffectsTex = std::make_unique<Texture>("resources/images/effects.png");
}

ImpactEffectManager::~ImpactEffectManager()
{
}

ImpactEffectManager & ImpactEffectManager::GetInstance()
{
	static ImpactEffectManager* pInstance = new ImpactEffectManager();
	return *pInstance;
}

void ImpactEffectManager::Draw() const
{
	for (Effect e : m_Effects)
	{
		m_upEffectsTex->Draw(Rectf(e.pos.x - 32.0f, e.pos.y - 32.0f, 64.0f, 64.0f), Rectf(128.0f * e.stage, (e.type + 1) * 128.0f, 128.0f, 128.0f));
	}
}

void ImpactEffectManager::Update(float elapsedSec)
{
	for (size_t i{ 0 }; i < m_Effects.size(); ++i)
	{
		m_Effects[i].elapsedTime += elapsedSec;
		if (m_Effects[i].elapsedTime > m_FrameTime)
		{
			++m_Effects[i].stage;
			m_Effects[i].elapsedTime = 0.0f;
		}
		if (m_Effects[i].stage == 7)
		{
			m_Effects.erase(m_Effects.begin() + i);
		}
	}
}

void ImpactEffectManager::AddEffect(Point2f pos, EffectType type)
{
	Effect newEffect;
	newEffect.elapsedTime = 0.0f;
	newEffect.pos = pos;
	newEffect.stage = 0;
	newEffect.type = type;
	m_Effects.push_back(newEffect);
}

void ImpactEffectManager::Clear()
{
	m_Effects.clear();
}


