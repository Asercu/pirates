//Arne Sercu
//1DAE17

#pragma once
#include "utils.h"
class Camera
{
public:
	explicit Camera(float Width, float height, float& scale);
	void SetBoundaries(Rectf boundaries);
	void Draw(const Rectf& toTrack);

	//Getters
	Point2f GetPosition(const Rectf & toTrack);
	Point2f GetPosition(const Point2f & toTrack);
	Rectf GetScreenBounds();
	Rectf GetSafeBox();

private:
	Point2f m_Pos;
	float m_Width;
	float m_Height;
	Rectf m_Boundaries;
	Point2f Track(const Rectf& toTrack);
	void Clamp(Point2f& bottomleftPos);
	float& m_Scale;
	float m_Margin = 50.0f;
};

