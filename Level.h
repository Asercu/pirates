//Arne Sercu
//1DAE17

#pragma once
#include "utils.h"
#include "ShipPart.h"
#include <vector>
#include "Island.h"

struct ShipCollisionInfo
{
	Point2f pos;
	Point2f center;
	float rotation;
	float width;
	float height;
};

class Level
{
public:
	Level();
	void Update(float elapsedsec, Point2f shipPos);
	void UpdateSea(float elapsedSec);
	Point2f HandleCollision(ShipCollisionInfo info) const;
	Vector2f GetWind() const;
	void Draw() const;
	void Reset();

	//GETTER
	std::vector<Island>& GetIslands();
	Rectf GetBoundaries() const;

private:
	Vector2f m_Wind;
	Rectf m_Boundaries;
	std::unique_ptr<Texture> m_upSea;
	Point2f m_SeaPosition;
	float m_SeaRotation;
	std::unique_ptr<Texture> m_upMapBorder;
	std::vector<Point2f> m_BorderVertices;
	std::vector<Island> m_Islands;
	void DrawSea() const;
	void AddIslands();
};

