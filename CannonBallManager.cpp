//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "CannonBallManager.h"


CannonBallManager::CannonBallManager()
	: m_SplashSound{ "resources/sounds/Splash.wav" }
	, m_BoomSound{ "resources/sounds/Boom.mp3" }
	, m_Cannonsound{ "resources/sounds/cannonsound.wav" }
{
}

CannonBallManager::~CannonBallManager()
{
	for (CannonBall* pB : m_ShipBalls)
	{
		delete pB;
	}
	for (CannonBall* pB : m_FortBalls)
	{
		delete pB;
	}
	for (CannonBall* pB : m_EnemyShipBalls)
	{
		delete pB;
	}
}

CannonBallManager & CannonBallManager::GetInstance()
{
	static CannonBallManager* pInstance = new CannonBallManager();
	return *pInstance;
}

void CannonBallManager::Update(float elapsedSec, std::vector<Island>& islands, Ship &ship, std::vector<std::shared_ptr<EnemyShip>>& enemyShips)
{
	for (CannonBall* pB : m_ShipBalls)
	{
		pB->Update(elapsedSec, islands, enemyShips);
	}
	for (CannonBall* pB : m_FortBalls)
	{
		pB->Update(elapsedSec, ship);
	}
	for (CannonBall* pB : m_EnemyShipBalls)
	{
		pB->Update(elapsedSec, ship, islands);
	}
	SetBallBounds(ship.GetShipCenter());
	Cleanup(islands);
	ImpactEffectManager::GetInstance().Update(elapsedSec);
}

void CannonBallManager::Draw() const
{
	for (CannonBall* pB : m_ShipBalls)
	{
		pB->Draw();
	}
	for (CannonBall* pB : m_FortBalls)
	{
		pB->Draw();
	}
	for (CannonBall* pB : m_EnemyShipBalls)
	{
		pB->Draw();
	}
	ImpactEffectManager::GetInstance().Draw();
}

void CannonBallManager::Cleanup(std::vector<Island>& islands)
{
	for (size_t i{0}; i < m_ShipBalls.size(); ++i)
	{
		if (!m_ShipBalls[i]->IsAlive())
		{
			ImpactEffectManager::GetInstance().AddEffect(m_ShipBalls[i]->GetPosition(), EffectType::boom);
			m_BoomSound.Play(0);
			delete m_ShipBalls[i];
			m_ShipBalls.erase(m_ShipBalls.begin() + i);
		}
		else if (m_ShipBalls[i]->GetDisplacement() > m_MaxDistance + rand() % 50)
		{
			bool inWater = true;
			for (size_t j = 0; j < islands.size(); ++j)
			{
				if (inWater)
				{
					if (utils::IsPointInPolygon(m_ShipBalls[i]->GetPosition(), islands[j].GetVertices()))
					{
						inWater = false;
					}
				}			
			}
			if (inWater)
			{
				ImpactEffectManager::GetInstance().AddEffect(m_ShipBalls[i]->GetPosition(), EffectType::splash);
				m_SplashSound.Play(0);
			}
			else
			{
				ImpactEffectManager::GetInstance().AddEffect(m_ShipBalls[i]->GetPosition(), EffectType::smoke);
			}	
			delete m_ShipBalls[i];
			m_ShipBalls.erase(m_ShipBalls.begin() + i);
		}
	}
	for (size_t i{ 0 }; i < m_FortBalls.size(); ++i)
	{
		if (!m_FortBalls[i]->IsAlive())
		{
			ImpactEffectManager::GetInstance().AddEffect(m_FortBalls[i]->GetPosition(), EffectType::boom);
			m_BoomSound.Play(0);
			delete m_FortBalls[i];
			m_FortBalls.erase(m_FortBalls.begin() + i);
		}
		else if (m_FortBalls[i]->GetDisplacement() > m_MaxDistance + rand() % 50)
		{
			bool inWater = true;
			for (size_t j = 0; j < islands.size(); ++j)
			{
				if (inWater)
				{
					if (utils::IsPointInPolygon(m_FortBalls[i]->GetPosition(), islands[j].GetVertices()))
					{
						inWater = false;
					}
				}
			}
			if (inWater)
			{
				ImpactEffectManager::GetInstance().AddEffect(m_FortBalls[i]->GetPosition(), EffectType::splash);
				m_SplashSound.Play(0);
			}
			else
			{
				ImpactEffectManager::GetInstance().AddEffect(m_FortBalls[i]->GetPosition(), EffectType::smoke);
			}
			delete m_FortBalls[i];
			m_FortBalls.erase(m_FortBalls.begin() + i);
		}
	}
	for (size_t i{ 0 }; i < m_EnemyShipBalls.size(); ++i)
	{
		if (!m_EnemyShipBalls[i]->IsAlive())
		{
			ImpactEffectManager::GetInstance().AddEffect(m_EnemyShipBalls[i]->GetPosition(), EffectType::boom);
			m_BoomSound.Play(0);
			delete m_EnemyShipBalls[i];
			m_EnemyShipBalls.erase(m_EnemyShipBalls.begin() + i);
		}
		else if (m_EnemyShipBalls[i]->GetDisplacement() > m_MaxDistance + rand() % 50)
		{
			bool inWater = true;
			for (size_t j = 0; j < islands.size(); ++j)
			{
				if (inWater)
				{
					if (utils::IsPointInPolygon(m_EnemyShipBalls[i]->GetPosition(), islands[j].GetVertices()))
					{
						inWater = false;
					}
				}
			}
			if (inWater)
			{
				ImpactEffectManager::GetInstance().AddEffect(m_EnemyShipBalls[i]->GetPosition(), EffectType::splash);
				m_SplashSound.Play(0);
			}
			else
			{
				ImpactEffectManager::GetInstance().AddEffect(m_EnemyShipBalls[i]->GetPosition(), EffectType::smoke);
			}
			delete m_EnemyShipBalls[i];
			m_EnemyShipBalls.erase(m_EnemyShipBalls.begin() + i);
		}
	}
}

void CannonBallManager::Fire(Point2f pos, Vector2f v, Source s)
{
	m_Cannonsound.Play(0);
	ImpactEffectManager::GetInstance().AddEffect(pos, EffectType::smoke);
	switch (s)
	{
	case Ships:
		m_ShipBalls.push_back(new CannonBall(pos, v, m_wpTex, m_BallWidth, m_BallHeight, m_BallOffsetX, m_BallOffsetY));
		break;
	case Fort:
		m_FortBalls.push_back(new CannonBall(pos, v, m_wpTex, m_BallWidth, m_BallHeight, m_BallOffsetX, m_BallOffsetY));
		break;
	case EnemyShips:
		m_EnemyShipBalls.push_back(new CannonBall(pos, v, m_wpTex, m_BallWidth, m_BallHeight, m_BallOffsetX, m_BallOffsetY));
		break;
	default:
		break;
	}
}

void CannonBallManager::SetBallData(std::shared_ptr<Texture> spTex, float width, float height, float offsetX, float offsetY)
{
	m_wpTex = spTex;
	m_BallWidth = width;
	m_BallHeight = height;
	m_BallOffsetX = offsetX;
	m_BallOffsetY = offsetY;
}

Rectf CannonBallManager::GetBallBounds() const
{
	return m_BallBounds;
}

void CannonBallManager::ClearAll()
{
	for (size_t i = 0; i < m_ShipBalls.size(); ++i)
	{
		delete m_ShipBalls[i];
	}
	m_ShipBalls.clear();
	for (size_t i = 0; i < m_FortBalls.size(); ++i)
	{
		delete m_FortBalls[i];
	}
	m_FortBalls.clear();
	for (size_t i = 0; i < m_EnemyShipBalls.size(); ++i)
	{
		delete m_EnemyShipBalls[i];
	}
	m_EnemyShipBalls.clear();
}

void CannonBallManager::SetBallBounds(Point2f ShipCenter)
{
	float bottomY = ShipCenter.y;
	float topY = ShipCenter.y;
	float leftX = ShipCenter.x;
	float rightX = ShipCenter.x;

	for (CannonBall* pB : m_ShipBalls)
	{
		Point2f ballPos = pB->GetPosition();
		if (ballPos.x < leftX) leftX = ballPos.x;
		if (ballPos.x > rightX) rightX = ballPos.x;
		if (ballPos.y < bottomY) bottomY = ballPos.y;
		if (ballPos.y > topY) topY = ballPos.y;
	}
	m_BallBounds = Rectf(leftX, bottomY, rightX - leftX, topY - bottomY);
}
