//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "PlayerShip.h"

PlayerShip::~PlayerShip()
{
}

PlayerShip::PlayerShip(int damageState, Color c)
	: Ship(damageState, c)
{
}

void PlayerShip::Update(Vector2f wind, float elapsedSec, const Level & level)
{
	//get input
	const Uint8 *pKeysStates = SDL_GetKeyboardState(nullptr);
	//rotate
	if (pKeysStates[SDL_SCANCODE_D])
	{
		SetRotation(m_Rotation - elapsedSec * m_ShipRotationSpeed);
	}
	if (pKeysStates[SDL_SCANCODE_A])
	{
		SetRotation(m_Rotation + elapsedSec * m_ShipRotationSpeed);
	}
	//accelerate and decellerate
	if (pKeysStates[SDL_SCANCODE_W])
	{
		if (!m_SpeedChange)	Accellerate();
		m_SpeedChange = true;
	}
	if (pKeysStates[SDL_SCANCODE_S])
	{
		if (!m_SpeedChange) Decellerate();
		m_SpeedChange = true;
	}
	if (!pKeysStates[SDL_SCANCODE_W] && !pKeysStates[SDL_SCANCODE_S])
	{
		m_SpeedChange = false;
	}
	//shooting
	if (pKeysStates[SDL_SCANCODE_E])
	{
		if (m_CooldownRight > m_CooldownTime)
		{
			m_PressedRight = true;
			SetShowRangeRight(true);
		}		
	}
	else
	{
		if (m_PressedRight)
		{
			FireRight();
			m_PressedRight = false;
			SetShowRangeRight(false);
		}
	}
	if (pKeysStates[SDL_SCANCODE_Q])
	{
		if (m_CooldownLeft > m_CooldownTime)
		{
			m_PressedLeft = true;
			SetShowRangeLeft(true);
		}
	}
	else
	{
		if (m_PressedLeft)
		{
			FireLeft();
			m_PressedLeft = false;
			SetShowRangeLeft(false);
		}
	}
	Ship::Update(wind, elapsedSec, level);
}
