//Arne Sercu
//1DAE17

#pragma once
#include <vector>
#include "Texture.h"
#include "FortCannon.h"

enum IslandType
{
	type1,
	type2,
	type3,
	type4
};

class Island
{
public:
	explicit Island(float x, float y, IslandType type);
	Island(const Island& i);
	void Draw() const;
	void Update(float elapsedsec, Point2f shipPos);
	void EraseCannon(int index);

	//GETTER
	std::vector<Point2f> GetVertices() const;
	Point2f GetPos() const;
	std::vector<Point2f> GetFortVertices() const;
	std::vector<FortCannon> &GetCannons();

private:
	std::unique_ptr<Texture> m_upTex;
	Point2f m_Pos;
	std::vector<Point2f> m_Vertices;
	std::unique_ptr<Texture> m_upFortTex;
	std::vector<Point2f> m_FortVertices;
	std::vector<FortCannon> m_FortCannons;
	IslandType m_Type;
};

