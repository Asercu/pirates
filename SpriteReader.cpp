//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "SpriteReader.h"
#include "pugixml.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <memory>

SpriteReader::SpriteReader()
{
}

SpriteReader::~SpriteReader()
{
}

SpriteReader & SpriteReader::GetInstance()
{
	static SpriteReader* pInstance = new SpriteReader();
	return *pInstance;
}

ShipPart SpriteReader::GetHull(int damageState)
{
	if (damageState < 1 || damageState > 3)
	{
		std::cerr << "GetHull: incorrect damage state, must be 1 2 or 3\n";
		return ShipPart();
	}

	std::ostringstream idStream;
	idStream << "hullLarge" << damageState << ".png";
	std::string id = idStream.str();

	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - GetHull";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == id)
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return ShipPart(returnValue);
			}
		}
		std::cerr << "Hull not found\n";
		return ShipPart();
	}
	else
	{
		return ShipPart(m_LookupTable[id]);
	}
}

ShipPart SpriteReader::GetSailSmall(int damageState, Color c)
{
	if (damageState < 1 || damageState > 3)
	{
		std::cerr << "GetHull: incorrect damage state, must be 1 2 or 3\n";
		return ShipPart();
	}

	std::ostringstream idStream;
	idStream << "sailSmall_" << c << damageState << ".png";
	std::string id = idStream.str();

	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - GetSailSmall";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == id)
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return ShipPart(returnValue);
			}
		}
	}
	else
	{
		return ShipPart(m_LookupTable[id]);
	}
	std::cerr << "Small Sail not found\n";
	return ShipPart();
}

ShipPart SpriteReader::GetSailLarge(int damageState, Color c)
{
	if (damageState < 1 || damageState > 3)
	{
		std::cerr << "GetHull: incorrect damage state, must be 1 2 or 3\n";
		return ShipPart();
	}

	std::ostringstream idStream;
	idStream << "sailLarge_" << c << damageState << ".png";
	std::string id = idStream.str();

	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - GetSailLarge";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == id)
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return ShipPart(returnValue);
			}
		}
	}
	else
	{
		return ShipPart(m_LookupTable[id]);
	}
	std::cerr << "Large Sail not found\n";
	return ShipPart();
}

ShipPart SpriteReader::GetNest()
{
	std::string id = "nest.png";

	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - GetNest";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == id)
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return ShipPart(returnValue);
			}
		}
	}
	else
	{
		return ShipPart(m_LookupTable[id]);
	}
	std::cerr << "Nest not found\n";
	return ShipPart();
}

ShipPart SpriteReader::GetMastTop(Color c)
{
	std::ostringstream idStream;
	idStream << "flag_" << c << ".png";
	std::string id = idStream.str();
	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - GetMastTop";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == id)
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return ShipPart(returnValue);
			}
		}
	}
	else
	{
		return ShipPart(m_LookupTable[id]);
	}
	std::cerr << "flag not found\n";
	return ShipPart();
}

ShipPart SpriteReader::GetCannon()
{
	std::string id = "cannonLoose.png";
	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - cannon";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == "cannonLoose.png")
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return ShipPart(returnValue);
			}
		}
	}
	else
	{
		return ShipPart(m_LookupTable[id]);
	}
	std::cerr << "cannon not found\n";
	return ShipPart();
}

SpriteInfo SpriteReader::GetCannonBallInfo()
{
	std::string id = "cannonBall.png";
	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - cannon";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == "cannonBall.png")
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return returnValue;
			}
		}
	}
	else
	{
		return m_LookupTable[id];
	}
	std::cerr << "cannonball not found\n";
	return SpriteInfo();
}

SpriteInfo SpriteReader::GetFortCannon()
{
	std::string id = "cannonMobile.png";
	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - cannon";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == "cannonMobile.png")
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return returnValue;
			}
		}
	}
	else
	{
		return m_LookupTable[id];
	}
	std::cerr << "cannonball not found\n";
	return SpriteInfo();
}

SpriteInfo SpriteReader::GetFullShip(Color c)
{
	std::ostringstream idStream;
	idStream << "ship_" << c << ".png";
	std::string id = idStream.str();

	if (m_LookupTable.find(id) == m_LookupTable.end())
	{
		SpriteInfo returnValue;
		pugi::xml_document doc;
		if (!doc.load_file("Resources/images/shipsMiscellaneous_sheet.xml")) std::cerr << "cannot read file - GetFullShip";
		pugi::xml_node atlas = doc.child("TextureAtlas");
		for (pugi::xml_node subTexture = atlas.first_child(); subTexture; subTexture = subTexture.next_sibling())
		{
			std::string str = subTexture.first_attribute().as_string();
			if (str == id)
			{
				std::cout << id << " found\n";
				returnValue.offsetX = subTexture.attribute("x").as_float();
				returnValue.offsetY = subTexture.attribute("y").as_float();
				returnValue.width = subTexture.attribute("width").as_float();
				returnValue.height = subTexture.attribute("height").as_float();
				m_LookupTable[id] = returnValue;
				return returnValue;
			}
		}
	}
	else
	{
		return m_LookupTable[id];
	}
	std::cerr << "flag not found\n";
	return SpriteInfo();
}


