//Arne Sercu
//1DAE17

#pragma once
#include "Texture.h"
#include "SpriteReader.h"
#include "ShipPart.h"

class HUD
{
public:
	explicit HUD(Point2f pos, float width, float height, Color sailColor, float& shipHealth,
		Point2f speedPos, float speedWidth, float speedHeight, float& shipSpeed, 
		Point2f pickupPos, float pickupWidth, float pickupHeight, float& pickupCountDown, PickupType& pickupType,
		Point2f timePos, float timeWidth, float timeHeight, float maxTime, float& time);
	void DrawHealthBar() const;
	void DrawSpeedBar() const;
	void DrawPickup() const;
	void DrawTimer() const;

private:
	std::shared_ptr<Texture> m_spTexture;
	ShipPart m_FullHealth;
	ShipPart m_TwothirdsHealth;
	ShipPart m_OneThirdHealth;
	std::unique_ptr<Texture> m_upDead;
	float m_HealthWidth;
	float m_HealthHeight;
	Point2f m_HealthPos;
	float& m_shipHealth;

	float& m_ShipSpeed;
	std::unique_ptr<Texture> m_upAnchor;
	float m_SpeedWidth;
	float m_SpeedHeight;
	Point2f m_SpeedPos;

	PickupType& m_CurrentPickup;
	float& m_PickupTimer;
	Point2f m_PickupPos;
	float m_PickupHeight;
	float m_PickupWidth;
	std::unique_ptr<Texture> m_upPickupTexture;

	Point2f m_TimePos;
	float m_TimeWidth;
	float m_TimeHeight;
	float m_MaxTime;
	float& m_Time;
	std::unique_ptr<Texture> m_upTimerTex;
};

