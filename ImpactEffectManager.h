//Arne Sercu
//1DAE17

#pragma once
#include <vector>
#include "Texture.h"

enum EffectType
{
	splash,
	boom,
	smoke
};

struct Effect
{
	Point2f pos;
	int stage;
	EffectType type;
	float elapsedTime;
};

class ImpactEffectManager
{
public:
	~ImpactEffectManager();
	static ImpactEffectManager& GetInstance();
	void Draw() const;
	void Update(float elapsedSec);
	void AddEffect(Point2f pos, EffectType type);
	void Clear();

private:
	ImpactEffectManager();
	std::vector<Effect> m_Effects;
	float m_FrameTime;
	std::unique_ptr<Texture> m_upEffectsTex;
};

