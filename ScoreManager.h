//Arne Sercu
//1DAE17

#pragma once
#include <map>

class ScoreManager
{
public:
	static ScoreManager& GetInstance();
	void Draw() const;
	void SetWindowWidth(float width);
	void SetWindowHeight(float height);
	void AddPoints(int points);
	void RemovePoints(int Points);
	void Reset();
	std::string GetScore() const;
	bool SaveScore(std::string name);
	std::multimap<int, std::string> GetHighScores() const;

private:
	ScoreManager();
	std::unique_ptr<Texture> m_upLabelText;
	float m_WindowWidth;
	float m_WindowHeight;
	int m_Score;
	void SetLabel();
	std::string m_ScoreString;
	std::string m_FontString = "Resources/Font/BlackSamsGold.ttf";
	std::string m_HSFile = "Resources/Files/Pirates_HS.txt";
	std::multimap<int, std::string> m_SavedScores;
	void ReadSavedScores();
};

