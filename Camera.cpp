//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "Camera.h"

Camera::Camera(float width, float height, float& scale)
	: m_Width{width}
	, m_Height{height}
	, m_Scale{ scale }
{
}

void Camera::SetBoundaries(Rectf boundaries)
{
	m_Boundaries = boundaries;
}

void Camera::Draw(const Rectf & toTrack)
{
	m_Pos = Track(toTrack);
	Clamp(m_Pos);
	utils::SetColor(Color4f(0.0f, 0.0f, 1.0f, 1.0f));
	utils::DrawRect(Rectf(m_Pos.x, m_Pos.y, m_Width, m_Height), 5.0f);
}

Point2f Camera::GetPosition(const Rectf & toTrack)
{
	m_Pos = Track(toTrack);
	Clamp(m_Pos);
	return m_Pos;
}

Point2f Camera::GetPosition(const Point2f & toTrack)
{
	m_Pos = Point2f(toTrack.x - m_Width / (2 * m_Scale), toTrack.y - m_Height / (2 * m_Scale));
	Clamp(m_Pos);
	return m_Pos;
}

Rectf Camera::GetScreenBounds()
{
	return (Rectf(m_Pos.x, m_Pos.y, m_Pos.x + m_Width / m_Scale, m_Pos.y + m_Height / m_Scale));
}

Rectf Camera::GetSafeBox()
{
	return Rectf(m_Pos.x + m_Margin / m_Scale, m_Pos.y + m_Margin / m_Scale, (m_Width - 2 * m_Margin) / m_Scale, (m_Height - 2 * m_Margin) / m_Scale);
}

Point2f Camera::Track(const Rectf & toTrack)
{
	return Point2f(toTrack.left + toTrack.width / 2 - m_Width / 2, toTrack.bottom + toTrack.height / 2 - m_Height / 2);
}

void Camera::Clamp(Point2f & bottomleftPos)
{
	if (bottomleftPos.x < m_Boundaries.left) m_Pos.x = m_Boundaries.left;
	if (bottomleftPos.x > m_Boundaries.left + m_Boundaries.width - m_Width / m_Scale) m_Pos.x = m_Boundaries.left + m_Boundaries.width - m_Width / m_Scale;
	if (bottomleftPos.y < m_Boundaries.bottom) m_Pos.y = m_Boundaries.bottom;
	if (bottomleftPos.y > m_Boundaries.bottom + m_Boundaries.height - m_Height / m_Scale) m_Pos.y = m_Boundaries.bottom + m_Boundaries.height - m_Height / m_Scale;
}
