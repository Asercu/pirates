//Arne Sercu
//1DAE17

#pragma once

struct Pickup
{
	Point2f position;
	PickupType type;
};

class PickUpManager
{
public:
	PickUpManager();
	void AddPickup(PickupType t, Point2f p);
	void Update(Ship& player);
	void Draw() const;
	void Reset();

private:
	std::vector<Pickup> m_Pickups;
	std::unique_ptr<Texture> m_upTex;
};

