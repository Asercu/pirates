//Arne Sercu
//1DAE17

#pragma once
#include "ShipPart.h"
#include "Texture.h"
#include "utils.h"
#include <vector>
#include <memory>
#include "SpriteReader.h"
#include "SoundEffect.h"
#include "Level.h"
#include <deque>

enum PickupType
{
	Heal,
	Speedboost,
	Rapidfire,
	Invincible,
	None
};

class Ship
{
public:
	//GENERAL
	virtual ~Ship();
	explicit Ship(int damageState, Color c);
	virtual void Draw() const;
	virtual void DrawTrail() const;
	virtual void Update(Vector2f wind, float elapsedSec, const Level& level);
	void Accellerate();
	void Decellerate();
	void FireRight();
	void FireLeft();
	virtual void HandleFiring(float elapsedSec);
	int TakeDamage();
	int Repair();
	void IsHit(float damage);
	void IsHealed(float heal);
	void PickupPickedUp(PickupType p);
	void Reset();
	
	//GETTERS
	Point2f GetPosition() const;
	Point2f GetShipCenter() const;
	std::shared_ptr<Texture> GetTexture() const;
	std::vector<Point2f> GetCollision() const;
	float& GetHealth();
	bool IsAlive() const;
	float& GetSpeed();
	PickupType& GetCurrentPickup();
	float& GetPickupTimer();
	int GetDamageState() const;

	//SETTERS
	void SetHull(ShipPart h);
	void SetSailSmall(ShipPart ss);
	void SetSailLarge(ShipPart sl);
	void SetNest(ShipPart n);
	void SetMast(ShipPart m);	
	void SetRotation(float angle);
	void SetSailRotation(float angle);
	void SetPosition(Point2f);
	void SetCannons(ShipPart c, int numCannons);
	void SetShowRangeRight(bool enable);
	void SetShowRangeLeft(bool enable);
	void SetColor(Color c);
	void SetAlive(bool value);

protected:
	bool m_Healthchange;
	float m_Rotation;
	float m_ShipRotationSpeed = 90.0f;
	bool m_SpeedChange = false;
	float m_CooldownLeft;
	float m_CooldownRight;
	float m_CooldownTime = 2.0f;
	float m_StandardCooldown = 2.0f;
	float m_RapidCooldown = 0.5f;
	std::vector<Point2f> m_RangeLeft;
	std::vector<Point2f> m_RangeRight;
	float m_Speed;
	float m_RightCannonSoundTimer;
	float m_LeftCannonSoundTimer;
	int m_RightCannonShotsLeft;
	int m_LeftCannonShotsLeft;
	float m_CannonSoundDelay = 0.05f;
	std::vector<Point2f> m_BallSpawnPointsRight;
	std::vector<Point2f> m_BallSpawnPointsLeft;
	Vector2f m_BulletVelocityLeft;
	Vector2f m_BulletVelocityRight;
	int m_DamageState;
	float m_CurHealth = 100.0f;
	float m_MaxSpeed = 100.0f;
	float m_StandardMaxSpeed = 100.0f;
	float m_BoostSpeed = 200.0f;
	Color m_Color;

private:
	//hp
	float m_MaxHealth = 100.0f;
	bool m_Alive = true;
	bool m_Dying = false;
	float m_DeathTimer = 0.0f;
	bool m_MustDraw = true;

	//SHIP PARTS
	std::shared_ptr<Texture> m_spTexture;
	ShipPart m_Hull;
	ShipPart m_SailSmall;
	ShipPart m_SailLarge;
	ShipPart m_Nest;
	ShipPart m_Mast;
	std::vector<ShipPart> m_LeftCannons;
	std::vector<ShipPart> m_RightCannons;
	std::vector<Point2f> m_CollisionPoly;
	std::vector<Point2f> m_OriginalCollisionPoly;
	
	//MOVEMENT AND DRAWING
	float m_SailAngle;
	Vector2f m_Direction;
	Point2f m_Position;
	Vector2f m_CurrentVelocity;

	
	//SHOOTING
	float m_CannonBallSpeed = 400.0f;
	bool m_ShowRangeRight = false;
	bool m_ShowRangeLeft = false;
	float m_RangePreviewOpacity;
	float m_ElapsedTime;
	std::vector<Point2f> m_OriginalBallSpawnPointsRight;
	std::vector<Point2f> m_OriginalBallSpawnPointsLeft;

	//OTHER
	SoundEffect m_DamageSound;
	SoundEffect m_RepairSound;

	//FUNCTIONS
	float GetWindAngle(Vector2f wind);
	void Move(Vector2f wind, float elapsedSec);
	
	//Trail
	std::deque<Point2f> m_Trail;

	//pickups
	PickupType m_ActivePickup;
	float m_PickupCountDown;
};
