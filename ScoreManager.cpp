//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "ScoreManager.h"
#include <sstream>
#include <iomanip>
#include <fstream>


ScoreManager::ScoreManager()
{
	SetLabel();
	ReadSavedScores();
}

void ScoreManager::SetLabel()
{
	std::stringstream strstream;
	strstream << std::setw(6) << std::setfill('0') << m_Score;
	m_ScoreString = strstream.str();
	m_upLabelText = std::make_unique<Texture>(m_ScoreString, m_FontString, 50, Color4f(0.0f, 0.0f, 0.0f, 1.0f));
}

void ScoreManager::ReadSavedScores()
{
	std::ifstream inStream{ m_HSFile };
	if (!inStream) std::cerr << "Error opening highscore file" << std::endl;
	while (inStream.good())
	{
		try
		{
			std::string line;
			std::getline(inStream, line);
			if (line.length() != 0)
			{
				size_t separator = line.find(',');
				std::string name = line.substr(0, separator);
				int points = std::stoi(line.substr(separator + 1, line.size() - separator));
				m_SavedScores.insert(std::make_pair(points, name));
			}
		}
		catch (std::exception e)
		{
			std::cerr << "Error reading highscores: " << e.what() << ". skipping line." << std::endl;
		}
	}
}

ScoreManager & ScoreManager::GetInstance()
{
	static ScoreManager* pInstance = new ScoreManager();
	return *pInstance;
}

void ScoreManager::Draw() const
{
	m_upLabelText->Draw(Point2f((m_WindowWidth - m_upLabelText->GetWidth()) / 2.0f, m_WindowHeight - 70.0f));
}

void ScoreManager::SetWindowWidth(float width)
{
	m_WindowWidth = width;
}

void ScoreManager::SetWindowHeight(float height)
{
	m_WindowHeight = height;
}

void ScoreManager::AddPoints(int points)
{
	m_Score += points;
	SetLabel();
}

void ScoreManager::RemovePoints(int Points)
{
	m_Score -= Points;
	if (m_Score < 0) m_Score = 0;
	SetLabel();
}

void ScoreManager::Reset()
{
	m_Score = 0;
	SetLabel();
}

std::string ScoreManager::GetScore() const
{
	return m_ScoreString;
}

bool ScoreManager::SaveScore(std::string name)
{
	m_SavedScores.insert(std::make_pair(m_Score, name));
	std::ofstream outStream{ m_HSFile };
	if (!outStream)
	{
		std::cerr << "Could not open highscore file" << std::endl;
		return false;
	}
	for (std::pair<int, std::string> entry : m_SavedScores)
	{
		outStream << entry.second << "," << entry.first << std::endl;
	}
	return true;
}

std::multimap<int, std::string> ScoreManager::GetHighScores() const
{
	return m_SavedScores;
}
