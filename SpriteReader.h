//Arne Sercu
//1DAE17

#pragma once
#include "ShipPart.h"
#include <map>

enum Color
{
	green,
	red,
	blue,
	yellow,
	gray
};

class SpriteReader
{
public:
	~SpriteReader();
	static SpriteReader& GetInstance();
	ShipPart GetHull(int damageState);
	ShipPart GetSailSmall(int damageState, Color c);
	ShipPart GetSailLarge(int damageState, Color c);
	ShipPart GetNest();
	ShipPart GetMastTop(Color c);
	ShipPart GetCannon();
	SpriteInfo GetCannonBallInfo();
	SpriteInfo GetFortCannon();
	SpriteInfo GetFullShip(Color c);

private:
	SpriteReader();
	std::map<std::string, SpriteInfo> m_LookupTable;
};

