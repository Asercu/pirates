//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "HUD.h"

HUD::HUD(Point2f posHealth, float widthHealth, float heightHealth, Color sailColor, float& shipHealth,
	Point2f speedPos, float speedWidth, float speedHeight, float& shipSpeed, 
	Point2f pickupPos, float pickupWidth, float pickupHeight, float& pickupCountDown, PickupType& pickupType,
	Point2f timePos, float timeWidth, float timeHeight, float maxTime, float& time)
	: m_HealthPos{ posHealth }
	, m_HealthWidth{ widthHealth }
	, m_HealthHeight{ heightHealth }
	, m_shipHealth{ shipHealth }
	, m_ShipSpeed{ shipSpeed }
	, m_SpeedPos { speedPos }
	, m_SpeedHeight{ speedHeight }
	, m_SpeedWidth{ speedWidth }
	, m_PickupTimer{ pickupCountDown }
	, m_CurrentPickup{ pickupType }
	, m_PickupPos{ pickupPos }
	, m_PickupWidth{ pickupWidth }
	, m_PickupHeight{ pickupHeight }
	, m_TimePos{ timePos }
	, m_TimeWidth{ timeWidth }
	, m_TimeHeight{ timeHeight }
	, m_MaxTime{ maxTime }
	, m_Time{ time }
{
	m_FullHealth = SpriteReader::GetInstance().GetSailLarge(1, sailColor);
	m_TwothirdsHealth = SpriteReader::GetInstance().GetSailLarge(2, sailColor);
	m_OneThirdHealth = SpriteReader::GetInstance().GetSailLarge(3, sailColor);
	m_spTexture = std::make_shared<Texture>("Resources/images/shipsMiscellaneous_sheet.png");
	m_upDead = std::make_unique<Texture>("Resources/images/skull.png");
	m_upAnchor = std::make_unique<Texture>("Resources/images/anchor.png");
	m_upPickupTexture = std::make_unique<Texture>("resources/images/pickups.png");
	m_upTimerTex = std::make_unique<Texture>("resources/images/TimerIcon.png");
}

void HUD::DrawHealthBar() const
{
	float actualbarwidth = m_HealthWidth - m_HealthHeight - 10.0f;
	utils::SetColor(Color4f(0.8f, 0.0f, 0.0f, 1.0f));
	utils::FillRect(m_HealthPos.x + m_HealthHeight, m_HealthPos.y + m_HealthHeight * 0.35f, actualbarwidth * (m_shipHealth / 100.0f >= 0 ? m_shipHealth / 100.0f : 0.0f), m_HealthHeight * 0.30f);
	utils::SetColor(Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	utils::DrawRect(m_HealthPos.x + m_HealthHeight, m_HealthPos.y + m_HealthHeight * 0.35f, actualbarwidth, m_HealthHeight * 0.30f, 5.0f);
	utils::SetColor(Color4f(1.0f, 0.0f, 0.0f, 1.0f));
	utils::FillEllipse(m_HealthPos.x + m_HealthHeight / 2, m_HealthPos.y + m_HealthHeight / 2, m_HealthHeight * 0.5f, m_HealthHeight * 0.5f);
	utils::SetColor(Color4f(0.0f, 0.0f, 0.0f, 1.0f));
	utils::DrawEllipse(m_HealthPos.x + m_HealthHeight / 2, m_HealthPos.y + m_HealthHeight / 2, m_HealthHeight * 0.5f, m_HealthHeight * 0.5f, 5.0f);
	m_upDead->Draw(Rectf(m_HealthPos.x + m_HealthHeight * 0.1f, m_HealthPos.y + m_HealthHeight * 0.1f, m_HealthHeight * 0.8f, m_HealthHeight * 0.8f));
	utils::DrawLine(m_HealthPos.x + m_HealthHeight + actualbarwidth / 3, m_HealthPos.y + m_HealthHeight * 0.2f, m_HealthPos.x + m_HealthHeight + actualbarwidth / 3, m_HealthPos.y + m_HealthHeight * 0.65f, 3.0f);
	utils::DrawLine(m_HealthPos.x + m_HealthHeight + actualbarwidth / 3 * 2, m_HealthPos.y + m_HealthHeight * 0.2f, m_HealthPos.x + m_HealthHeight + actualbarwidth / 3 * 2, m_HealthPos.y + m_HealthHeight * 0.65f, 3.0f);
	utils::DrawLine(m_HealthPos.x + m_HealthHeight + actualbarwidth, m_HealthPos.y + m_HealthHeight * 0.2f, m_HealthPos.x + m_HealthHeight + actualbarwidth, m_HealthPos.y + m_HealthHeight * 0.65f, 3.0f);	
	m_spTexture->Draw(Rectf(m_HealthPos.x + m_HealthHeight + actualbarwidth / 3 - m_OneThirdHealth.GetWidth() / 4.0f, m_HealthPos.y + m_HealthHeight * 0.2f - m_OneThirdHealth.GetHeight() / 2, m_OneThirdHealth.GetWidth() / 2, m_OneThirdHealth.GetHeight() / 2), m_OneThirdHealth.GetSrcRect());
	m_spTexture->Draw(Rectf(m_HealthPos.x + m_HealthHeight + actualbarwidth / 3 * 2 - m_TwothirdsHealth.GetWidth() / 4.0f, m_HealthPos.y + m_HealthHeight * 0.2f - m_TwothirdsHealth.GetHeight() / 2, m_TwothirdsHealth.GetWidth() / 2, m_TwothirdsHealth.GetHeight() / 2), m_TwothirdsHealth.GetSrcRect());
	m_spTexture->Draw(Rectf(m_HealthPos.x + m_HealthHeight + actualbarwidth - m_FullHealth.GetWidth() / 4.0f, m_HealthPos.y + m_HealthHeight * 0.2f - m_FullHealth.GetHeight() / 2, m_FullHealth.GetWidth() / 2, m_FullHealth.GetHeight() / 2), m_FullHealth.GetSrcRect());
}

void HUD::DrawSpeedBar() const
{
	float actualbarwidth = m_SpeedWidth - 2 * m_SpeedHeight;
	utils::SetColor(Color4f(0.490196f, 0.713725f, 0.788235f, 1.0f));
	utils::FillRect(m_SpeedPos.x + m_SpeedHeight, m_SpeedPos.y + m_SpeedHeight * 0.35f, actualbarwidth * m_ShipSpeed / 3.0f, m_SpeedHeight * 0.30f);
	utils::SetColor(Color4f(1.0f, 1.0f, 1.0f, 1.0f));
	utils::DrawRect(m_SpeedPos.x + m_SpeedHeight, m_SpeedPos.y + m_SpeedHeight * 0.35f, actualbarwidth, m_SpeedHeight * 0.30f, 5.0f);
	utils::SetColor(Color4f(0.690196f, 0.913725f, 0.988235f, 1.0f));
	utils::FillEllipse(m_SpeedPos.x + m_SpeedHeight / 2, m_SpeedPos.y + m_SpeedHeight / 2, m_SpeedHeight * 0.5f, m_SpeedHeight * 0.5f);
	utils::SetColor(Color4f(1.0f, 1.0f, 1.0f, 1.0f));
	utils::DrawEllipse(m_SpeedPos.x + m_SpeedHeight / 2, m_SpeedPos.y + m_SpeedHeight / 2, m_SpeedHeight * 0.5f, m_SpeedHeight * 0.5f, 5.0f);
	utils::SetColor(Color4f(0.690196f, 0.913725f, 0.988235f, 1.0f));
	utils::FillEllipse(m_SpeedPos.x + m_SpeedWidth - m_SpeedHeight / 2, m_SpeedPos.y + m_SpeedHeight / 2, m_SpeedHeight * 0.5f, m_SpeedHeight * 0.5f);
	utils::SetColor(Color4f(1.0f, 1.0f, 1.0f, 1.0f));
	utils::DrawEllipse(m_SpeedPos.x + m_SpeedWidth - m_SpeedHeight / 2, m_SpeedPos.y + m_SpeedHeight / 2, m_SpeedHeight * 0.5f, m_SpeedHeight * 0.5f, 5.0f);
	m_upAnchor->Draw(Rectf(m_SpeedPos.x + m_SpeedHeight * 0.1f, m_SpeedPos.y + m_SpeedHeight * 0.1f, m_SpeedHeight * 0.8f, m_SpeedHeight * 0.8f));
	m_spTexture->Draw(Rectf(m_SpeedPos.x + m_SpeedWidth - m_SpeedHeight / 2 - m_FullHealth.GetWidth() / 6.0f, m_SpeedPos.y + m_SpeedHeight / 2 - m_FullHealth.GetHeight() / 6.0f, m_FullHealth.GetWidth() / 3.0f, m_FullHealth.GetHeight() / 3.0f), m_FullHealth.GetSrcRect());
	utils::DrawLine(m_SpeedPos.x + m_SpeedHeight + actualbarwidth / 3, m_SpeedPos.y + m_SpeedHeight * 0.35f, m_SpeedPos.x + m_SpeedHeight + actualbarwidth / 3, m_SpeedPos.y + m_SpeedHeight * 0.65f, 3.0f);
	utils::DrawLine(m_SpeedPos.x + m_SpeedHeight + actualbarwidth / 3 * 2, m_SpeedPos.y + m_SpeedHeight * 0.35f, m_SpeedPos.x + m_SpeedHeight + actualbarwidth / 3 * 2, m_SpeedPos.y + m_SpeedHeight * 0.65f, 3.0f);
}

void HUD::DrawPickup() const
{
	if (m_CurrentPickup == PickupType::None) return;

	utils::SetColor(Color4f(0.2f, 0.2f, 0.2f, 1.0f));
	utils::FillRect(m_PickupPos.x + m_PickupWidth / 3.0f, m_PickupPos.y + m_PickupWidth, m_PickupWidth / 3.0f, (m_PickupHeight - m_PickupWidth) / 10.0f * m_PickupTimer);
	utils::SetColor(Color4f(0.5f, 0.5f, 0.5f, 1.0f));
	utils::DrawRect(m_PickupPos.x + m_PickupWidth / 3.0f, m_PickupPos.y + m_PickupWidth, m_PickupWidth / 3.0f, (m_PickupHeight - m_PickupWidth), 5.0f);
	m_upPickupTexture->Draw(Rectf(m_PickupPos.x, m_PickupPos.y, m_PickupWidth, m_PickupWidth), Rectf(128.0f * m_CurrentPickup, 0.0f, 128.0f, 128.0f));
}

void HUD::DrawTimer() const
{
	utils::SetColor(Color4f(1.0f, 1.0f, 1.0f, 1.0f));
	utils::FillEllipse(m_TimePos.x + m_TimeWidth / 2.0f, m_TimePos.y + m_TimeWidth / 2.0f, m_TimeWidth / 2.0f, m_TimeWidth / 2.0f);
	utils::SetColor(Color4f(0.5f, 0.5f, 0.5f, 1.0f));
	utils::DrawEllipse(m_TimePos.x + m_TimeWidth / 2.0f, m_TimePos.y + m_TimeWidth / 2.0f, m_TimeWidth / 2.0f, m_TimeWidth / 2.0f, 5.0f);
	m_upTimerTex->Draw(Rectf(m_TimePos.x + m_TimeWidth * 0.1f, m_TimePos.y + m_TimeWidth * 0.1f, m_TimeWidth * 0.8f, m_TimeWidth * 0.8f));
	utils::SetColor(Color4f(0.2f, 0.2f, 0.2f, 1.0f));
	utils::FillRect(m_TimePos.x + m_TimeWidth / 3.0f, m_TimePos.y + m_TimeWidth, m_TimeWidth / 3.0f, (m_TimeHeight - m_TimeWidth) / m_MaxTime * m_Time);
	utils::SetColor(Color4f(0.5f, 0.5f, 0.5f, 1.0f));
	utils::DrawRect(m_TimePos.x + m_TimeWidth / 3.0f, m_TimePos.y + m_TimeWidth, m_TimeWidth / 3.0f, (m_TimeHeight - m_TimeWidth), 5.0f);
}
