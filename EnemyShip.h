//Arne Sercu
//1DAE17

#pragma once
#include "Ship.h"

class EnemyShip : public Ship
{
public:
	~EnemyShip();
	explicit EnemyShip(int damageState);
	EnemyShip(const EnemyShip& e);
	void Update(Vector2f wind, float elapsedSec, Level& level, Point2f playerPos, std::vector<std::shared_ptr<EnemyShip>> enemyShips);
	void Draw() const override;
	virtual void HandleFiring(float elapsedSec) override;

private:
	Point2f m_LeftAnticrashOriginal;
	Point2f m_RightAnticrashOriginal;
	Point2f m_LeftAnticrash;
	Point2f m_RightAnticrash;
};

