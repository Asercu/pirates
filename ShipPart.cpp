//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "ShipPart.h"

ShipPart::ShipPart()
{
}

ShipPart::ShipPart(SpriteInfo& si)
{
	m_Info = si;
}

float ShipPart::GetOffsetX() const
{
	return m_Info.offsetX;
}

float ShipPart::GetOffsetY() const
{
	return m_Info.offsetY;
}

float ShipPart::GetWidth() const
{
	return m_Info.width;
}

float ShipPart::GetHeight() const
{
	return m_Info.height;
}

Rectf ShipPart::GetSrcRect() const
{
	return Rectf(m_Info.offsetX, m_Info.offsetY + m_Info.height, m_Info.width, m_Info.height);
}