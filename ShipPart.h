//Arne Sercu
//1DAE17

#pragma once
#include "Texture.h"

class ShipPart
{
public:
	ShipPart();
	explicit ShipPart(SpriteInfo& si);
	float GetOffsetX() const;
	float GetOffsetY() const;
	float GetWidth() const;
	float GetHeight() const;
	Rectf GetSrcRect() const;

private:
	SpriteInfo m_Info;
};

