//Arne Sercu
//1DAE17

#include "stdafx.h"
#include "FortCannon.h"
#include "utils.h"
#include "CannonBallManager.h"

FortCannon::FortCannon() {};

FortCannon::FortCannon(Point2f pos, float range)
	: m_Pos{ pos }
	, m_Range{ pos, range }
	, m_spTexture{ std::make_shared<Texture>("Resources/images/shipsMiscellaneous_sheet.png") }
	, m_Angle{ 0.0f }
	, m_CollisionBox{}
{
	m_SI = SpriteReader::GetInstance().GetFortCannon();
	m_CollisionBox.push_back(Point2f(pos.x - 35.0f, pos.y - 35.0f));
	m_CollisionBox.push_back(Point2f(pos.x + 35.0f, pos.y - 35.0f));
	m_CollisionBox.push_back(Point2f(pos.x + 35.0f, pos.y + 35.0f));
	m_CollisionBox.push_back(Point2f(pos.x - 35.0f, pos.y + 35.0f));
}

void FortCannon::Draw() const
{
	glPushMatrix();
	glTranslatef(m_Pos.x, m_Pos.y, 0.0f);
	glRotatef(m_Angle, 0.0f, 0.0f, 1.0f);
	glTranslatef(-m_SI.width / 2.0f - m_Pos.x, -m_SI.height / 2.0f - m_Pos.y, 0.0f);
	m_spTexture->Draw(Rectf(m_Pos.x, m_Pos.y, m_SI.width, m_SI.height), Rectf(m_SI.offsetX, m_SI.offsetY + m_SI.height, m_SI.width, m_SI.height));
	glPopMatrix();
}

void FortCannon::Update(float elapsedsec, Point2f shipPos)
{
	if (utils::IsPointInCircle(shipPos, m_Range))
	{
		RotateToPoint(shipPos, elapsedsec);
		Vector2f toship = Vector2f(m_Pos, shipPos);
		float test = Vector2f(1.0f, 0.0f).AngleWith(toship) * 180.0f / 3.14159565f;
		if (abs(test - m_Angle) < 5.0f)
		{
			Fire();
		}
	}
	m_CurrentCannonCooldown += elapsedsec;
}

void FortCannon::SetPosition(Point2f pos)
{
	m_Pos = pos;
	m_Range = Circlef(m_Pos, m_Range.radius);
	m_CollisionBox = std::vector<Point2f>();
	m_CollisionBox.push_back(Point2f(pos.x - 28.0f, pos.y - 28.0f));
	m_CollisionBox.push_back(Point2f(pos.x + 28.0f, pos.y - 28.0f));
	m_CollisionBox.push_back(Point2f(pos.x + 28.0f, pos.y + 28.0f));
	m_CollisionBox.push_back(Point2f(pos.x - 28.0f, pos.y + 28.0f));
}

Point2f FortCannon::GetPosition() const
{
	return m_Pos;
}

std::vector<Point2f> FortCannon::GetCollision() const
{
	return m_CollisionBox;
}

void FortCannon::Die()
{
	for (int i = 0; i < 10; ++i)
	{
		ImpactEffectManager::GetInstance().AddEffect(Point2f(m_Pos.x - 28.0f + rand() % 56, m_Pos.y - 28.0f + rand() % 56), EffectType::boom);
	}
}

void FortCannon::Fire()
{
	if (m_CurrentCannonCooldown >= m_CannonCooldown)
	{
		CannonBallManager::GetInstance().Fire(m_Pos, Vector2f(std::cos(m_Angle * 3.14159565f / 180.0f), std::sin(m_Angle * 3.14159565f / 180.0f)).Normalized() * m_CannonBallSpeed, Source::Fort);
		m_CurrentCannonCooldown = 0.0f;
	}	
}

void FortCannon::RotateToPoint(Point2f shipPos, float elapsedsec)
{
	Vector2f toship = Vector2f(m_Pos, shipPos);
	float angle2 = Vector2f(1.0f, 0.0f).AngleWith(toship) * 180.0f / 3.14159565f;
	if (angle2 > 360.0f) angle2 -= 360.0f;
	if (angle2 < 0.0f) angle2 += 360.0f;
	if (m_Angle > 360.0f) 
		m_Angle -= 360.0f;
	if (m_Angle < 0.0f) 
		m_Angle += 360.0f;
	//current aimkwadrant, current shipkwadrant
	std::pair<int, int> kwadrants;
	if (m_Angle < 90.0f) kwadrants.first = 1;
	else if (m_Angle < 180.0f) kwadrants.first = 2;
	else if (m_Angle < 270.0f) kwadrants.first = 3;
	else kwadrants.first = 4;
	if (angle2 < 90.0f) kwadrants.second = 1;
	else if (angle2 < 180.0f) kwadrants.second = 2;
	else if (angle2 < 270.0f) kwadrants.second = 3;
	else kwadrants.second = 4;

	if (kwadrants.first == kwadrants.second)
	{
		if (m_Angle > angle2)
		{
			m_Angle -= m_Rotationspeed * elapsedsec;
		}
		else
		{
			m_Angle += m_Rotationspeed * elapsedsec;
		}
	}
	else if (kwadrants.second - kwadrants.first == 1 || (kwadrants.first == 4 && kwadrants.second == 1))
	{
		m_Angle += m_Rotationspeed * elapsedsec;
	}
	else if (kwadrants.first - kwadrants.second == 1 || (kwadrants.second == 4 && kwadrants.first == 1))
	{
		m_Angle -= m_Rotationspeed * elapsedsec;
	}
	else
	{
		if (abs((m_Angle + 360.0f) - angle2) > abs((angle2 - m_Angle)))
		{
			m_Angle += m_Rotationspeed * elapsedsec;
		}
		else
		{
			m_Angle -= m_Rotationspeed * elapsedsec;
		}
	}
}

