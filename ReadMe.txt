Pirates!

In this arcade style pirate game the goal is to achieve the highest possible score before the time runs out.
Points are scored by destroying the cannons on the forts and the enemy ships on the sea. Certain powerups can be collected to give the player a slight advantage such as higher speed, lower cooldown on the cannons and healing.

This game was made as a school project using C++ and the DAE game engine which uses SDL.
The sprites used for this project come from http://kenney.nl/