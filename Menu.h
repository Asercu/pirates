//Arne Sercu
//1DAE17

#pragma once
#include "SoundEffect.h"

enum MenuActions
{
	Play,
	Quit,
	Nothing
};

enum ActiveMenu
{
	Main,
	LearnMovement,
	LearnFire,
	LearnPickup,
	Highscores,
	GameOver
};

class Menu
{
public:
	explicit Menu(float windowWidth, float windowHeight);
	void DrawMainMenu() const;
	void Update(Point2f mousePos);
	std::pair<MenuActions, Color> OnClick();
	void SetActiveMenu(ActiveMenu a);

private:
	//Text textures
	std::unique_ptr<Texture> m_upTitle;
	std::unique_ptr<Texture> m_upArrowRight;
	std::unique_ptr<Texture> m_upArrowRightHover;
	std::unique_ptr<Texture> m_upArrowLeft;
	std::unique_ptr<Texture> m_upArrowLeftHover;
	std::unique_ptr<Texture> m_upPlay;
	std::unique_ptr<Texture> m_upPlayHover;
	std::unique_ptr<Texture> m_upControlsTitle;
	std::unique_ptr<Texture> m_upControlsMovement;
	std::unique_ptr<Texture> m_upControlsFire;
	std::unique_ptr<Texture> m_upLearnToPlay;
	std::unique_ptr<Texture> m_upLearnToPlayHover;
	std::unique_ptr<Texture> m_upMainMenu;
	std::unique_ptr<Texture> m_upMainMenuHover;
	std::unique_ptr<Texture> m_upControlsPickups;
	std::unique_ptr<Texture> m_upQuit;
	std::unique_ptr<Texture> m_upQuitHover;
	std::unique_ptr<Texture> m_upHighscores;
	std::unique_ptr<Texture> m_upHighscoresHover;
	std::unique_ptr<Texture> m_upHighscoresTitle;
	std::unique_ptr<Texture> m_upGameOverTitle;
	std::unique_ptr<Texture> m_upRetry;
	std::unique_ptr<Texture> m_upRetryHover;
	std::unique_ptr<Texture> m_upObtainedScore;
	std::unique_ptr<Texture> m_upSaveScore;
	std::unique_ptr<Texture> m_upSaved;
	std::unique_ptr<Texture> m_upSaveScoreHover;
	std::unique_ptr<Texture> m_upSaveScoreDisabled;
	std::unique_ptr<Texture> m_upPlayerName;
	std::unique_ptr<Texture> m_upScore;
	std::unique_ptr<Texture> m_upNoName;
	std::vector<std::unique_ptr<Texture>> m_upHighScoreTextures;
	std::shared_ptr<Texture> m_spShipsheet;
	
	//positions and position helper variables
	float m_WindowWidth;
	float m_WindowHeight;
	Rectf m_ArrowRightDest;
	Rectf m_ArrowLeftDest;
	Rectf m_PlayDest;
	Rectf m_LearnToPlayDest;
	Rectf m_HighScoresDest;
	Rectf m_QuitDest;
	Rectf m_LearnArrowRightDest;
	Rectf m_MainMenuDest;
	Rectf m_LearnArrowLeftDest;
	Rectf m_GameOverMainMenuDest;
	Rectf m_GameOverRetryDest;
	Rectf m_GameOverScoreDest;
	Rectf m_GameOverSaveScoreDest;
	Point2f m_GameOverHighScore1Pos;
	Point2f m_HighscoreHighScore1Pos;
	
	//functions
	void RefreshHighScores();
	void SaveScore();
	void ColorUp();
	void ColorDown();

	//other
	size_t m_HighscorePage = 0;
	size_t m_NumHighScorePages;
	SoundEffect m_ClickSound;
	SpriteInfo m_ShipInfo;
	Color m_ActiveColor;
	std::string m_FontString;
	Point2f m_MousePos;
	ActiveMenu m_CurrentMenu = ActiveMenu::Main;
	std::string m_LongplayerName;
	bool m_ReadLetter = false;
	std::string m_PlayerName = "";
	bool m_Saved = false;
	
};

