//Arne Sercu
//1DAE17

#pragma once
#include "SpriteReader.h"
#include "Texture.h"
#include <memory>
#include <vector>

class FortCannon
{
public:
	FortCannon();
	explicit FortCannon(Point2f pos, float range);
	void Draw() const;
	void Update(float elapsedsec, Point2f shipPos);	
	void Die();

	//GETTER
	Point2f GetPosition() const;
	std::vector<Point2f> GetCollision() const;
	//SETTER
	void SetPosition(Point2f pos);


private:
	Point2f m_Pos;
	Circlef m_Range;
	SpriteInfo m_SI;
	std::shared_ptr<Texture> m_spTexture;
	float m_Rotationspeed = 90.0f;
	float m_Angle;
	float m_CannonBallSpeed = 300.0f;
	float m_CannonCooldown = 2.0f;
	float m_CurrentCannonCooldown = 2.0f;
	void Fire();
	void RotateToPoint(Point2f shipPos, float elapsedsec);
	bool hasfired = false;
	std::vector<Point2f> m_CollisionBox;
};

