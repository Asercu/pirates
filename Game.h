//Arne Sercu
//1DAE17

#pragma once
#include "SpriteReader.h"
#include "ShipPart.h"
#include "PlayerShip.h"
#include "utils.h"
#include "Camera.h"
#include "SoundStream.h"
#include "ImpactEffectManager.h"
#include "Level.h"
#include "HUD.h"
#include "ScoreManager.h"
#include "EnemyShip.h"
#include "Menu.h"

enum GameState
{
	GamePlay,
	InMenu
};

class Game
{
public:
	explicit Game( const Window& window );
	Game( const Game& other ) = delete;
	Game& operator=( const Game& other ) = delete;
	~Game();

	void Update( float elapsedSec );
	void Draw( );
	void StartGame();

	// Event handling
	void ProcessKeyDownEvent( const SDL_KeyboardEvent& e );
	void ProcessKeyUpEvent( const SDL_KeyboardEvent& e );
	void ProcessMouseMotionEvent( const SDL_MouseMotionEvent& e );
	void ProcessMouseDownEvent( const SDL_MouseButtonEvent& e );
	void ProcessMouseUpEvent( const SDL_MouseButtonEvent& e );

private:
	// DATA MEMBERS
	Window m_Window;
	Camera m_Camera;
	PlayerShip m_Ship;
	float m_TimeLimit;
	float m_MaxTime;
	HUD m_Hud;
	float m_CameraScale;
	float m_OriginalCameraScale;
	SoundStream m_Music;
	SoundStream m_MenuMusic;
	Level m_Level;
	float m_ZoomSpeed = 0.30f;
	PickUpManager m_PickupManager;
	Texture m_MenuBackground;
	GameState m_State;
	Menu m_Menu;
	Point2f m_MousePos;
	std::vector<std::shared_ptr<EnemyShip>> m_spEnemyShips;

	// FUNCTIONS
	void Initialize( );
	void Cleanup( );
	void ClearBackground( );
	void TimesUp();
};