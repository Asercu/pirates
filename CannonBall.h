//Arne Sercu
//1DAE17

#pragma once
#include "utils.h"
#include "Texture.h"
#include <memory>
#include "Island.h"
#include "Ship.h"
#include "EnemyShip.h"

enum Source
{
	Ships,
	Fort,
	EnemyShips
};

class CannonBall
{
public:
	CannonBall(Point2f pos, Vector2f v, std::weak_ptr<Texture> spTex, float width, float height, float offsetX, float offSetY);
	void Update(float elapsedSec, std::vector<Island>& islands, std::vector<std::shared_ptr<EnemyShip>>& enemyShips);
	void Update(float elapsedSec, Ship &ship);
	void Update(float elapsedSec, Ship &ship, std::vector<Island>& islands);
	void Draw() const;

	//GETTER
	float GetDisplacement() const;
	Point2f GetPosition() const;
	bool IsAlive() const;

private:
	Point2f m_Position;
	Vector2f m_Velocity;
	Point2f m_StartPosition;
	std::weak_ptr<Texture> m_wpTex;
	float m_Width;
	float m_Height;
	float m_OffsetX;
	float m_OffsetY;
	bool m_Alive;
};

