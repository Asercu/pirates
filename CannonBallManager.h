//Arne Sercu
//1DAE17

#pragma once
#include "CannonBall.h"
#include <memory>
#include "ImpactEffectManager.h"
#include "SoundEffect.h"
#include "Island.h"
#include "EnemyShip.h"

class CannonBallManager
{
public:
	~CannonBallManager();
	static CannonBallManager& GetInstance();
	void Update(float elapsedSec, std::vector<Island>& islands, Ship &ship, std::vector<std::shared_ptr<EnemyShip>>& enemyShips);
	void Draw() const;
	void Cleanup(std::vector<Island>& islands);
	void Fire(Point2f pos, Vector2f v, Source s);
	void SetBallData(std::shared_ptr<Texture> spTex, float width, float height, float offsetX, float offsetY);
	Rectf GetBallBounds() const;
	void ClearAll();

private:
	CannonBallManager();
	std::vector<CannonBall*> m_ShipBalls;
	std::vector<CannonBall*> m_FortBalls;
	std::vector<CannonBall*> m_EnemyShipBalls;

	float m_MaxDistance = 500.0f;
	std::weak_ptr<Texture> m_wpTex;
	float m_BallWidth;
	float m_BallHeight;
	float m_BallOffsetX;
	float m_BallOffsetY;
	SoundEffect m_Cannonsound;
	SoundEffect m_SplashSound;
	SoundEffect m_BoomSound;
	Rectf m_BallBounds;

	void SetBallBounds(Point2f ShipCenter);
};

